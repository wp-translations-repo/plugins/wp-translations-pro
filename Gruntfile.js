module.exports = function(grunt) {

	var user        = grunt.option('user'),
		  token       = grunt.option('token')


	// Load multiple grunt tasks using globbing patterns
	require('load-grunt-config')(grunt);

  grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		makepot: {
			target: {
				options: {
					cwd: '',
					exclude: ['assets/.*', 'node_modules/.*', 'vendor/.*'],
					domainPath: '/languages',
					include: [],
					mainFile: 'wp-translations-pro.php',
					potComments: '',
					potFilename: 'wp-translations-pro.pot',
					potHeaders: {
							poedit: true,
							'x-poedit-keywordslist': true
					},
					processPot: null,
					type: 'wp-plugin',
					updateTimestamp: true,
					updatePoFiles: false
				}
			}
		},
		sass: {
      dist: {

      },
      dev: {
				options: {
          style: "expanded",
					sourcemap: "none",
					noCache: true
        },
        files: [
          {
            expand: true,
            cwd: "assets/scss/",
            src: ["*.scss"],
            dest: "assets/css",
            ext: ".css"
          }
        ]
			}
    },
		uglify: {
      compile: {
				expand: true,
          ext: '.min.js',
          src: [
              'assets/js/*.js',

              // Exclusions
              '!assets/js/*.min.js',
					]
      }
    },
		cssmin: {
      compile: {
          expand: true,
          ext: '.min.css',
          src: [
              'assets/css/*.css',
              // Exclusions
              '!css/*.min.css',
          ]
      }
		},
		watch: {
      styles: {
        files: "assets/scss/*.scss",
        tasks: ["styles:dev"]
      }
    },
		replace: {
			readme: {
				src: [ 'readme.txt' ],
				overwrite: true,
				replacements: [{
					from: /Stable tag: (.*)/,
					to: "Stable tag: <%= pkg.version %>"
				},{
					from: /Tested up to: (.*)/,
					to: "Tested up to: <%= pkg.upto %>"
				}]
			},
			main: {
				src: [ '<%= pkg.constant.slug %>.php' ],
				overwrite: true,
				replacements: [{
					from: / Version:\s*(.*)/,
					to: " Version:      <%= pkg.version %>"
				},{
					from: / Text Domain:\s*(.*)/,
					to: " Text Domain: <%= pkg.constant.textdomain %>"
				}, {
					from: / @version    \s*(.*)/,
					to: " @version:    <%= pkg.version %>"
				}, {
					from: / 'WPTPRO_VERSION',                       \s*(.*)/,
					to: " '<%= pkg.constant.upperslug %>_VERSION',                       '<%= pkg.version %>' );"
				}]
			},
			endpoint: {
				src: [ 'builds/builds.json' ],
				overwrite: true,
				replacements: [{
					from: / "new_version":\s*(.*)/,
					to: ' "new_version": "<%= pkg.version %>",'
				}, {
					from: / "tested": (.*)/,
					to: ' "tested": "<%= pkg.upto %>",'
				}, {
					from: / "last_updated": (.*)/,
					to: ' "last_updated": "' + new Date().toISOString() + '",'
				}, {
					from: / "package": (.*)/,
					to: ' "package": "https://gitlab.com/wp-translations-repo/plugins/<%= pkg.constant.slug %>/raw/master/builds/<%= pkg.version %>/<%= pkg.constant.slug %>.zip",'
				}, {
					from: / "1x": (.*)/,
					to: ' "1x": "https://gitlab.com/wp-translations-repo/plugins/<%= pkg.constant.slug %>/raw/master/builds/assets/icon-128x128.png",'
				}, {
					from: / "default": (.*)/,
					to: ' "default": "https://gitlab.com/wp-translations-repo/plugins/<%= pkg.constant.slug %>/raw/master/builds/assets/icon-128x128.png"'
				}
				]
			}
		},
		compress: {
			build: {
				options: {
	        archive: 'builds/<%= pkg.version %>/<%= pkg.constant.slug %>.zip'
	      },
	      files: [
	        { src: 'src/**', dest: '<%= pkg.constant.slug %>/' },
	        { src: 'assets/js/**', dest: '<%= pkg.constant.slug %>/'  },
	        { src: 'assets/css/**', dest: '<%= pkg.constant.slug %>/'  },
	        { src: 'assets/img/**', dest: '<%= pkg.constant.slug %>/'  },
					{ src: 'vendor/**', dest: '<%= pkg.constant.slug %>/'  },
	        { src: 'languages/**', dest: '<%= pkg.constant.slug %>/'  },
	        { src: 'templates/**', dest: '<%= pkg.constant.slug %>/'  },
	        { src: 'readme.txt', dest: '<%= pkg.constant.slug %>/'  },
	        { src: '<%= pkg.constant.slug %>.php', dest: '<%= pkg.constant.slug %>/'  },
	        { src: 'uninstall.php', dest: '<%= pkg.constant.slug %>/'  }
				]
			},
			release: {
				options: {
	        archive: 'builds/stable/<%= pkg.constant.slug %>.zip'
	      },
	      files: [
	        { src: 'src/**', dest: '<%= pkg.constant.slug %>/' },
	        { src: 'assets/js/**', dest: '<%= pkg.constant.slug %>/'  },
	        { src: 'assets/css/**', dest: '<%= pkg.constant.slug %>/'  },
	        { src: 'assets/img/**', dest: '<%= pkg.constant.slug %>/'  },
					{ src: 'vendor/**', dest: '<%= pkg.constant.slug %>/'  },
	        { src: 'languages/**', dest: '<%= pkg.constant.slug %>/'  },
	        { src: 'templates/**', dest: '<%= pkg.constant.slug %>/'  },
	        { src: 'readme.txt', dest: '<%= pkg.constant.slug %>/'  },
	        { src: '<%= pkg.constant.slug %>.php', dest: '<%= pkg.constant.slug %>/'  },
	        { src: 'uninstall.php', dest: '<%= pkg.constant.slug %>/'  }
				]
			}
		}

	});

	grunt.registerTask("default", ["dev", "watch"]);
  grunt.registerTask("dev", ["styles:dev"]);
	grunt.registerTask("dist", ["styles:dist", "scripts:dist", "makepot"]);
  grunt.registerTask("bump", ["replace:readme", "replace:main"]);
  grunt.registerTask("build", ["compress:build"]);
  grunt.registerTask("release", ["dist", "bump", "build", "compress:release", "replace:endpoint"]);

  grunt.registerTask("scripts:dist", ["uglify:compile"]);

  grunt.registerTask("styles:dev", ["sass:dev"]);
  grunt.registerTask("styles:dist", ["cssmin:compile"]);

};
