=== WP-Translations Pro ===
Contributors: WP-Translations Team, G3ronim0, fxbenard
Tags: Translations, i18n, Translate, Localization, l10n
Requires at least: 4.6
Tested up to: 5.9.2
Requires PHP: 5.6
Stable tag: 1.2.10
License: GPL V2

== Description ==
The easiest way to manage the French Translations of your favorite WordPress themes and plugins provided by FX Bénard.

== Installation ==

= 1. The easy way =
* Download the plugin (.zip file) by using the blue "download" button underneath the plugin banner at the top
* In your WordPress dashboard, navigate to Plugins > Add New
* Click on "Upload Plugin"
* Upload the .zip file
* Activate the plugin
* A new `FXB-Translations` menu is available in your WordPress dashboard

= 2. The old-fashioned and reliable way (FTP) =
* Download the plugin (.zip file) by using the blue "download" button underneath the plugin banner at the top
* Extract the archive and then upload, via FTP, the `wp-translations-pro` folder to the `<WP install folder>/wp-content/plugins/` folder on your host
* Activate the plugin
* A new `FXB-Translations` menu is available in your WordPress dashboard

== Changelog ==

= 1.2.10 =
* Add support for EDD Software Licensing plugin
* Tested up to WordPress 5.9.2

= 1.2.9 =
* Fix change deprecated .live() JQuery function by .on()
* Fix getVersion null result after deleting license

= 1.2.8 =
* Add support for Monarch plugin

= 1.2.7 =
* Performance improvement
* Fix clear updates cache
* Some mirror fix

= 1.2.6 =
* Fix empty response from store
* Add button "check for new products" in licenses page

= 1.2.5 =
* Fix warning in licenses page
* Fix empty products transient
* Add upgrader routine

= 1.2.4 =
* Fix disappearing products
* Fix typo
* Update pot

= 1.2.3 =
* Add support for FacetWP
* Fix warning in update-core.php

= 1.2.2 =
* Fix Infinite update for Divi Builder
* Base updates on unique resources

= 1.2.1 =
* Fix Infinite update for Extra

= 1.2.0 =
* Fix AJAX result
* Update translation

= 1.1.8 =
* Fix modal in themes page
* Fix CSS
* Update translation

= 1.1.7 =
* Migration wizard

= 1.1.6 =
* Fix translations installation on license activation
* New assets

= 1.1.5 =
* Add sub menu pages
* Fix translations update notifications
* Add french Translations
* Auto purge logs monthly

= 1.1.4 =
* Fix translations update notifications
* Fix plugin footer propagation
*
= 1.1.3 =
* Switch from Gulp to Grunt

= 1.1.2 =
* Fix i18n issues
* Revamping helper classes

= 1.1.1 =
* Fix minification error

= 1.1 =
* Switch to Fxbenard trademark

= 1.0.3 =
* Enhancement: Change tabs system with a a11y library
* Enhancement: Remove thickbox for readme modal
* Enhancement: New debug UX (still in beta)
* Feature: Add count translations updates in adminbar updates link title for screen readers
* Feature: Uninstall process
* BugFix: Infinite update for translations with multiple resources
* Bugfix: Updates log
* Update: .pot file

= 1.0.2 =
* Mask license key in logs
* Code consistency

= 1.0.1 =
* Divi clear template cache
* Fix Product readme tabs styles and typo
* Fix notices conflicts on plugins.php

= 1.0.0 =
* Add plugin self updater
* Multisite support

= 0.0.9 =
* Change Gulp strategy
