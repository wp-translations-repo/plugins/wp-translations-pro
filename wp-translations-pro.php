<?php
/**
 * FXB-Translations
 *
 * @package     FXB-Translations
 * @author      WP-Translations Team
 *
 * @copyright   2017 WP-Translations Team
 * @license     http://creativecommons.org/licenses/GPL/2.0/ GNU General Public License, version 2 or higher
 *
 * @wordpress-plugin
 * Plugin Name: FXB-Translations
 * Description: The easiest way to manage the French Translations of your favorite WordPress themes and plugins provided by FX Bénard

 * Version:      1.2.10
 * Author:      WP-Translations Team
 * Author URI:  https://wp-translations.pro
 * Text Domain: wp-translations-pro
 * Domain Path: /languages
 * Copyright:   2018 WP-Translations Team
 */

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

require_once( dirname( __FILE__ ) . '/vendor/autoload.php' );

define( 'WPTPRO_VERSION',                       '1.2.10' );
define( 'WPTPRO_BASE_FILE',                     plugin_basename( __FILE__ ) );
define( 'WPTPRO_SLUG',                          'wp-translations-pro' );
define( 'WPTPRO_PLUGIN_NAME',                   'WP-Translations Pro' );
define( 'WPTPRO_PLUGIN_PATH',                   plugin_dir_path( __FILE__ ) );
define( 'WPTPRO_PLUGIN_URL',                    plugin_dir_url( __FILE__ ) );
define( 'WPTPRO_PLUGIN_DIR_TEMPLATES',          WPTPRO_PLUGIN_PATH . 'templates' );
define( 'WPTPRO_PLUGIN_DIR_TEMPLATES_ADMIN',    WPTPRO_PLUGIN_DIR_TEMPLATES . '/admin' );
define( 'WPTPRO_CONTENT_DIR',                   WP_CONTENT_DIR . '/wp-translations-pro' );
define( 'WPTPRO_CONTENT_PATH_TEMP',             WP_CONTENT_DIR . '/wp-translations-pro/temp' );
define( 'WPTPRO_CONTENT_URL_TEMP',              WP_CONTENT_URL . '/wp-translations-pro/temp' );
define( 'WPTPRO_PHP_VERSION_MIN',              '5.6' );

use WP_Translations_Pro\Models\HooksInterface;
use WP_Translations_Pro\Models\ActivationInterface;

use WP_Translations_Pro\WordPress\Helpers\FileHelper;
use WP_Translations_Pro\WordPress\Helpers\WizardHelper;

use WP_Translations_Pro\WordPress\Admin\i18n;
use WP_Translations_Pro\WordPress\Admin\EnqueueAssets;
use WP_Translations_Pro\WordPress\Admin\Notice;
use WP_Translations_Pro\WordPress\Admin\LicenseActions;
use WP_Translations_Pro\WordPress\Admin\SettingsActions;
use WP_Translations_Pro\WordPress\Admin\TranslationSetup;
use WP_Translations_Pro\WordPress\Admin\TranslationUpdater;
use WP_Translations_Pro\WordPress\Admin\TranslationUpgrader;
use WP_Translations_Pro\WordPress\Admin\UpdateCore;
use WP_Translations_Pro\WordPress\Admin\Page;
use WP_Translations_Pro\WordPress\Admin\PluginUpdater;
use WP_Translations_Pro\WordPress\Admin\SetupWizard;
use WP_Translations_Pro\WordPress\Cron\LogCron;
use WP_Translations_Pro\ThirdParty\ElegantThemes;

class WP_Translations_Pro implements HooksInterface {

	protected $actions = array();
	protected $slug;

	/**
	 * @param array $actions
	 */
	public function __construct( $actions = array() ) {
		$this->actions = $actions;
		$this->slug    = WPTPRO_SLUG;
 	}

	/**
	 * @return boolean
	 */
	protected function canLoaded() {

    if ( version_compare( phpversion(), WPTPRO_PHP_VERSION_MIN, '<=' ) ) {
        return array(
            "success" => false,
            "message" =>  sprintf(
                __( '<strong>%1$s</strong> requires PHP %2$s minimum, your website is actually running version %3$s.', 'wp-translations-pro' ),
                'FXB-Translations', '<code>' . WPTPRO_PHP_VERSION_MIN . '</code>', '<code>' . phpversion() . '</code>'
            )
        );
    }

		return array(
				"success" => true
		);
	}

	/**
	 * Execute when plugin not loaded
	 */
	public function notLoaded( $params ){
		if ( current_filter() !== 'activate_' . WPTPRO_BASE_FILE ) {
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			deactivate_plugins( WPTPRO_BASE_FILE, true );
		}

		$args = array(
			'back_link' => admin_url( 'plugins.php' )
		);

		wp_die( $params["message"], false, $args );
	}


	/**
	 * Execute plugin
	 */
	public function execute() {
		$result = $this->canLoaded();
		if( ! $result["success"] ) {
			$this->notLoaded( $result );
			return;
		} else {
			add_action( 'plugins_loaded',      array( $this, 'hooks' ), 0 );
			register_activation_hook(__FILE__, array( $this, 'executePlugin') );
		}
	}

	public function executePlugin() {

    switch ( current_filter() ) {
      case 'activate_' . $this->slug . '/' . $this->slug . '.php':
          foreach ( $this->getActions() as $key => $action ) {
              if ( $action instanceOf ActivationInterface ) {
                  $action->activation();
              }
          }
          break;
    }

  }


	/**
	 * @return array
	 */
	public function getActions() {
		return $this->actions;
	}

	/**
	 * Implements hooks from HooksInterface
	 *
	 * @see WP_Translations_Pro\Models\HooksInterface
	 *
	 * @return void
	 */
	public function hooks() {

		foreach ( $this->getActions() as $key => $action ) {

			switch( true ) {
				case $action instanceof HooksAdminInterface:
					if ( is_admin() ) {
						$action->hooks();
					}
					break;
				case $action instanceof HooksInterface:
					$action->hooks();
					break;
			}

		}

	}

}

$actions = array(
    new i18n(),
    new EnqueueAssets(),
    new ElegantThemes(),
    new Notice(),
    new LicenseActions(),
    new SettingsActions(),
    new TranslationSetup(),
    new TranslationUpdater(),
    new TranslationUpgrader(),
    new UpdateCore(),
    new Page(),
    new PluginUpdater(),
		new LogCron(),
		new SetupWizard()
);

$wp_translations_pro = new WP_Translations_Pro( $actions );
$wp_translations_pro->execute();

/**
 * Delete Options
 */
function wptproDeactivationHook() {
	wp_clear_scheduled_hook( 'wpt_pro_purge_logs' );
}
register_deactivation_hook( __FILE__, 'wptproDeactivationHook' );
