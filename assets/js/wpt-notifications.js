jQuery( function( $ ) {

	$( function() {

    if ( '1' === wpt_update_ajax.themes_updates ) {
      $( '.theme:not(.add-new-theme)' ).each( function() {

          var slug       = $(this).attr('data-slug');
          var updateSlug = $(this).attr('data-slug').toLowerCase();

          if( wpt_update_ajax.themes_translations.hasOwnProperty( slug ) ) {

            var langs = wpt_update_ajax['themes_translations'][''+slug+'']['updates'].join('|');
            var button =' <button id="wp-translations-update-' + slug + '" class="button-link" type="button" data-locale="' + langs + '" data-type="themes" data-slug="' + slug + '">' +  wpt_update_ajax.update_button + '</button>'
            if ( $( '.theme[data-slug="'+ slug +'"] .notice' ).length ) {
              $( '.theme[data-slug="'+ slug +'"] .notice').append( '<p>' + wpt_update_ajax.update_message + ' ' + langs + '</p>' );
            } else {
              $( '.theme[data-slug="'+ slug +'"]').append('<div id="wp-translations-notice-' + slug + '" class="wp-translations-update-message wp-translations-notice wp-translations-notice-warning notice notice-alt"><p>' +  wpt_update_ajax.update_message + '' + langs + ' - ' + button + '</p></div>');
            }

          }
          if( wpt_update_ajax.themes_products.hasOwnProperty( updateSlug ) ) {
            var button = '<button class="js-modal wpt-button-link button-link" data-modal-content-id="wpt-readme-' + updateSlug + '-fr_FR" data-modal-prefix-class="wpt-modal">' + wpt_update_ajax.view_details + '</button>'
            if ( $( '.theme[data-slug="'+ slug +'"] .notice' ).length ) {
              $( '.theme[data-slug="'+ slug +'"]').append('<div id="wp-translations-notice-' + updateSlug + '" class="wp-translations-info-message wp-translations-notice notice wp-translations-notice-info notice-alt wp-translations-notice-second"><p>' +  wpt_update_ajax.promote_message + ' - ' + button + '</p></div>');
            } else {
              $( '.theme[data-slug="'+ slug +'"]').append('<div id="wp-translations-notice-' + updateSlug + '" class="wp-translations-info-message wp-translations-notice notice wp-translations-notice-info notice-alt"><p>' +  wpt_update_ajax.promote_message + ' - ' + button + '</p></div>');
            }
          }

      });
    }
  });

	$( function() {
    if ( '1' == wpt_update_ajax.plugins_updates ) {
      $( '.wp-translations-update-row' ).each( function() {

        var list = $( this );
        $( list ).prev().addClass('update');

      });
    }
  });


  $( "[id^=wp-translations-update-]" ).on( "click", function(e) {
    e.preventDefault();

    var slug       = $(this).attr('data-slug');
    var type       = $(this).attr('data-type');
    var locale     = $(this).attr('data-locale');
    var updateSlug = $(this).attr('data-slug').toLowerCase();

    $.ajax({
      type: "POST",
      url: wpt_update_ajax.ajaxurl,
      data: {
        'action' : 'upgradeTranslation',
        'nonce'  : wpt_update_ajax.nonce,
        'slug'   : slug,
        'type'   : type,
        'locale' : locale,
      },
      beforeSend: function(reponse) {

          $( '[data-slug="'+ slug +'"]' ).addClass('updated');
          $( '#wp-translations-notice-' + slug ).addClass( 'updating-message' );

      },
    })

    .done( function( response, textStatus, jqXHR ) {

			$( '[data-slug="'+ slug +'"]' ).removeClass('updated');
			$( '#wp-translations-notice-' + slug )
			.removeClass( 'wp-translations-notice-warning updating-message' )
			.addClass( 'wp-translations-updated-message wp-translations-notice-success' );


			$.each( response.data.message, function( index, value ) {
				$( '#wp-translations-notice-' + slug ).html('<p>' +	value + '</p>' ).fadeOut(1000);
			});

    });

  });

});
