jQuery( function( $ ) {

  $('#table_licenses_logs').DataTable({
       "pagingType"  : "simple",
       "lengthChange": false,
       "searching"   : false,
       "info"        :      false,
       "order"       : [[ 0, "desc" ]],
       "language"    : {
        "sProcessing"    : wpt_license_ajax.i18n.datatable.sProcessing,
        "sSearch"        : wpt_license_ajax.i18n.datatable.sSearch,
        "sLengthMenu"    : wpt_license_ajax.i18n.datatable.sLengthMenu,
        "sInfo"          : wpt_license_ajax.i18n.datatable.sInfo,
        "sInfoEmpty"     : wpt_license_ajax.i18n.datatable.sInfoEmpty,
        "sInfoFiltered"  : wpt_license_ajax.i18n.datatable.sInfoFiltered,
        "sLoadingRecords": wpt_license_ajax.i18n.datatable.sLoadingRecords,
        "sZeroRecords"   : wpt_license_ajax.i18n.datatable.sZeroRecords,
        "sEmptyTable"    : wpt_license_ajax.i18n.datatable.sEmptyTable,
        "oPaginate"      : {
            "sFirst"   : wpt_license_ajax.i18n.datatable.sFirst,
            "sPrevious": wpt_license_ajax.i18n.datatable.sPrevious,
            "sNext"    : wpt_license_ajax.i18n.datatable.sNext,
            "sLast"    : wpt_license_ajax.i18n.datatable.sLast,
        },
        "oAria": {
            "sSortAscending" :  wpt_license_ajax.i18n.datatable.sSortAscending,
            "sSortDescending": wpt_license_ajax.i18n.datatable.sSortDescending,
        }
      }
   });

  $('#table_updates_logs').DataTable({
       "pagingType":   "simple",
       "lengthChange": false,
       "searching":    false,
       "info":         false,
       "order":        [[ 0, "desc" ]],
       "language": {
        "sProcessing":     wpt_license_ajax.i18n.datatable.sProcessing,
        "sSearch":         wpt_license_ajax.i18n.datatable.sSearch,
        "sLengthMenu":     wpt_license_ajax.i18n.datatable.sLengthMenu,
        "sInfo":           wpt_license_ajax.i18n.datatable.sInfo,
        "sInfoEmpty":      wpt_license_ajax.i18n.datatable.sInfoEmpty,
        "sInfoFiltered":   wpt_license_ajax.i18n.datatable.sInfoFiltered,
        "sLoadingRecords": wpt_license_ajax.i18n.datatable.sLoadingRecords,
        "sZeroRecords":    wpt_license_ajax.i18n.datatable.sZeroRecords,
        "sEmptyTable":     wpt_license_ajax.i18n.datatable.sEmptyTable,
        "oPaginate": {
            "sFirst":      wpt_license_ajax.i18n.datatable.sFirst,
            "sPrevious":   wpt_license_ajax.i18n.datatable.sPrevious,
            "sNext":       wpt_license_ajax.i18n.datatable.sNext,
            "sLast":       wpt_license_ajax.i18n.datatable.sLast,
        },
        "oAria": {
            "sSortAscending":  wpt_license_ajax.i18n.datatable.sSortAscending,
            "sSortDescending": wpt_license_ajax.i18n.datatable.sSortDescending,
        }
      }
   });


	// Premium Spinner
   var spinner = function ( slug, locale, type ) {
     html = '<span id="wpt-spinner-' + locale + '-'+ slug + '" class="wpt-spinner is-' + type + ' alignright"></span>';
     return html;
   }

   // Premium Button Save
   var save_button = function( slug, name, locale, type ) {
     html = '<button type="button" class="wpt-button" data-type="'+ type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" title="' + wpt_license_ajax.i18n.btn.save + '" id="save-license-' + slug + '"><span class="dashicons dashicons-plus-alt"></span>' + wpt_license_ajax.i18n.btn.save + '</button>';
     return html;
   };

   // Premium Button Delete
   var delete_button = function( slug, name, locale, type ) {
     html = '<button type="button" class="wpt-button danger" data-type="'+ type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" title="' + wpt_license_ajax.i18n.btn.deleted + '" id="delete-license-' + slug + '"><span class="dashicons dashicons-dismiss"></span><span class="screen-reader-text">' + wpt_license_ajax.i18n.btn.deleted + '</span></button>';
     return html;
   };

   // Premium Button Delete Plugin
   var delete_plugin_button = function() {
     html = '<button type="button" class="wpt-button danger" title="' + wpt_license_ajax.i18n.btn.deleted_plugin + '" id="delete-plugin"><span class="dashicons dashicons-dismiss"></span><span class="screen-reader-text">' + wpt_license_ajax.i18n.btn.deleted_plugin + '</span></button>';
     return html;
   };

   // Premium Button Activate
   var activate_button = function( slug, name, locale, type ) {
     html = '<button type="button" class="wpt-button" data-type="'+ type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" title="' + wpt_license_ajax.i18n.btn.activate + '" id="activate-license-' + slug + '"><span class="dashicons dashicons-controls-play"></span> ' + wpt_license_ajax.i18n.btn.activate + '</button>';
     return html;
   };

   // Premium Button Activate
   var activate_button_wizard = function( slug, name, locale, type, license ) {
     html = '<button type="button" class="wpt-button" data-type="'+ type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" data-license="' + license + '" title="' + wpt_license_ajax.i18n.btn.activate + '" id="activate-license-wizard-' + slug + '"><span class="dashicons dashicons-controls-play"></span> ' + wpt_license_ajax.i18n.btn.activate + '</button>';
     return html;
   };

   // Premium Button Deactivate
   var deactivate_button = function( slug, name, locale, type ) {
     html = '<button type="button" class="wpt-button" data-type="'+ type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" title="' + wpt_license_ajax.i18n.btn.deactivate + '" id="deactivate-license-' + slug + '"><span class="dashicons dashicons-controls-pause"></span> ' + wpt_license_ajax.i18n.btn.deactivate + '</button>';
     return html;
   };

   // Premium Button Check
   var check_button = function( slug, name, locale, type ) {
     html = '<button type="button" class="wpt-button" data-type="'+ type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" title="' + wpt_license_ajax.i18n.btn.check + '" id="check-license-' + slug + '"><span class="dashicons dashicons-update"></span><span class="screen-reader-text">' + wpt_license_ajax.i18n.btn.check + '</span></button>';
     return html;
   };

   // Premium Renew link
   var renew_link = function( slug, locale, license, data ) {
     html = '<a href="' + wpt_license_ajax.stores[locale].url + wpt_license_ajax.stores[locale].checkout +'?edd_license_key=' + license + '&download_id=' + wpt_license_ajax.products[locale][slug].id + '" id="wpt-renew-' + locale + '-' + slug + '" class="wpt-button" target="_blank"><span class="dashicons dashicons-image-rotate"></span> ' + wpt_license_ajax.i18n.btn.renew + '</a>';
     return html;
   }

   // Premium Upgrade Link
   var upgrade_link = function( slug, locale, license, data, upgrade_id ) {
     html = '<a href="' + wpt_license_ajax.stores[locale].url + wpt_license_ajax.stores[locale].checkout +'?edd_action=sl_license_upgrade&license_id=' + data.license_id + '&upgrade_id=' + upgrade_id + '" id="wpt-upgrade-' + locale + '-' + slug + '" class="wpt-button" target="_blank"><span class="dashicons dashicons-unlock"></span> ' + wpt_license_ajax.i18n.btn.upgrade + '</a>';
     return html;
   }

   // Premium Button Install
   var install_button = function( slug, name, locale, type ) {
     html = '<button type=button class="wpt-button wpt-install-package" data-type="' + type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" ><span class="dashicons dashicons-download"></span> ' + wpt_license_ajax.i18n.btn.download + '</button>';
     return html;
   }

   // Defaut notices values
   var notice  = 'info';
   var color   = '#00A0D2';

   // Premium Save License
   $( "[id^=wpt-col-actions-]" ).on( "click", "[id^=save-license-]", function(e) {
     e.preventDefault();

     var name    = $( this ).attr( 'data-name' );
     var slug    = $( this ).attr( 'data-slug' );
     var locale  = $( this ).attr( 'data-locale' );
     var type    = $( this ).attr( 'data-type' );
     var license = $( '#wpt-license-' + locale + '-' + slug ).val();

     $.ajax({
       type: "POST",
       url : wpt_license_ajax.ajaxurl,
       data: {
         'action'  : 'saveLicense',
         'nonce'   : wpt_license_ajax.nonce,
         'name'    : name,
         'slug'    : slug,
         'type'    : type,
         'locale'  : locale,
         'license' : license,
       },
       beforeSend: function(response) {
         $( '#wpt-spinner-' + locale + '-' + slug ).addClass( 'is-active' );
         $( '#wpt-row-notice-' + locale + '-' + slug )
           .show()
           .html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.saving + '</td>' )
       },
     })

     .done( function( response, textStatus, jqXHR ) {

       if ( true === response.success ) {
         var notice = 'success';
         var color  = '#46b450';

         $( '#wpt-col-actions-' + locale + '-' + slug ).html(
           activate_button( slug, name, locale, type ) +
           delete_button( slug, name, locale, type )
         );
       } else {
         var notice = 'error';
         var color = '#DC3232';

       }
       $( '#wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' ).addClass( 'is-' + notice );
       $( '#wpt-row-notice-' + locale + '-' + slug )
         .addClass( 'is-' + notice )
         .html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + response.data.message + '</td>' ).fadeOut(1000);
     });
   });

   // Premium Delete License
   $( "[id^=wpt-col-actions-]" ).on( "click", "[id^=delete-license-]", function(e) {
     e.preventDefault();

     var slug    = $( this ).attr( 'data-slug' );
     var name    = $( this ).attr( 'data-name' );
     var type    = $( this ).attr( 'data-type' );
     var locale  = $( this ).attr( 'data-locale' );
     var license = $( '#wpt-license-' + locale + '-' + slug ).val();

     $.ajax({
       type: "POST",
       url : wpt_license_ajax.ajaxurl,
       data: {
         'action'  : 'deleteLicense',
         'nonce'   : wpt_license_ajax.nonce,
         'slug'    : slug,
         'type'    : type,
         'locale'  : locale,
         'license' : license,
       },
       beforeSend: function(response) {
         $( '#wpt-spinner-' + locale + '-' + slug ).addClass( 'is-active' );
         $( '#wpt-row-notice-' + locale + '-' + slug )
           .show()
           .html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.deleting + '</td>' );
       },
     })

     .done( function( response, textStatus, jqXHR ) {

       $( '#wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' ).addClass( 'is-success' );
       $( '#wpt-license-' + locale + '-' + slug ).val('');
       $( '#wpt-col-actions-' + locale + '-' + slug ).html( save_button( slug, name, locale, type ) );
       $( '#wpt-col-status-' + locale + '-' + slug ).html('');
       $( '#wpt-col-activations-' + locale + '-' + slug ).html('');
       $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-success">' + spinner( slug, locale, notice ) + response.data.message + '</td>' );
     })
     .then( function( response, textStatus, jqXHR ) {

       var notice = 'info';
       var color  = '#00A0D2';

       $.ajax({
         type: "POST",
         url: wpt_license_ajax.ajaxurl,
         data: {
           'action': 'clearCacheUpdate',
           'nonce'  : wpt_license_ajax.nonce,
           'type'   : type,
         },
         beforeSend: function(response2) {
           $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.clear_cache + '</td>' );
         }
       })

       .done( function( response2, textStatus, jqXHR ) {
         var notice = 'success';
         var color  = '#46b450';
         $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + response.data.message + '</td>' ).fadeOut(1000);
       });
     });
   });

   // Premium Activate License
   $( "[id^=wpt-col-actions-]" ).on( "click", "[id^=activate-license-]", function(e) {
     e.preventDefault();

     var slug    = $( this ).attr( 'data-slug' );
     var name    = $( this ).attr( 'data-name' );
     var locale  = $( this ).attr( 'data-locale' );
     var type    = $( this ).attr( 'data-type' );
     var license = $( '#wpt-license-' + locale + '-' + slug ).val();
     var notice  = 'info';
     var color   = '#00A0D2';

     $.ajax({
       type: "POST",
       url : wpt_license_ajax.ajaxurl,
       data: {
         'action'  : 'activateLicense',
         'nonce'   : wpt_license_ajax.nonce,
         'slug'    : slug,
         'type'    : type,
         'locale'  : locale,
         'license' : license,
       },
       beforeSend: function(response) {
         $( '#wpt-spinner-' + locale + '-' + slug ).addClass( 'is-active' );
         $( '#wpt-row-license-' + locale + '-' + slug + ' .column-ajax' ).addClass( 'is-' + notice );
         $( '#wpt-row-notice-' + locale + '-' + slug )
           .show()
           .html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.check_license + '</td>' );
       },
     })

     .done( function( response, textStatus, jqXHR ) {

       if ( 'valid' == response.data.api.license ) {
				var license_limit = ( 'unlimited' == response.data.api.activations_left ) ? wpt_license_ajax.i18n.status.unlimited : response.data.api.license_limit;

				$( '#wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' ).addClass( 'is-success' );
				$( '#wpt-col-status-' + locale + '-' + slug ).html( '<p>' + wpt_license_ajax.i18n.status.activated + '</p>' );
				$( '#wpt-col-activations-' + locale + '-' + slug ).html( '<p>' + response.data.api.site_count + '/' + license_limit + '</p>' );

       } else {

         var notice = 'error';
         var color = '#DC3232';

         if ( 'expired' == response.data.api.license || 'expired' == response.data.api.error ) {
           $( '#wpt-col-actions-' + locale + '-' + slug ).append(
             renew_link( slug, locale, license )
           );
         }
         if ( 'no_activations_left' == response.data.api.error ) {

           var upgrade_id = parseInt( response.data.api.price_id ) + 1;

           if ( upgrade_id <= wpt_license_ajax.products[locale][slug].pricing ) {
             $( '#wpt-col-actions-' + locale + '-' + slug ).append( upgrade_link( slug, locale, license, response.data.api, upgrade_id ) );
           }

         }

       }

       $( '#wpt-col-actions-' + locale + '-' + slug ).remove( '#activate-license-' + slug );
       $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + response.data.message + '</td>' );
       if( 'error' == notice ) {
         return;
       }
     })
     .then( function( response, textStatus, jqXHR ) {
       if( false == response.success ) {
         return;
       }

			 $.ajax({
	        type: "POST",
	        url : wpt_license_ajax.ajaxurl,
	        data: {
	          'action'  : 'installTranslations',
	          'nonce'   : wpt_license_ajax.nonce,
	          'type'    : type,
	          'slug'    : slug,
	          'locale'  : locale,
	          'url'     : response.data.version.package,
	        },
	        beforeSend: function(response) {
	          $( '#wpt-spinner-' + locale + '-' + slug ).addClass( 'is-active' );
	          $( '#wpt-row-license-' + locale + '-' + slug + ' .column-ajax' ).addClass( 'is-' + notice );
	          $( '#wpt-row-notice-' + locale + '-' + slug )
	            .show()
	            .html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.download_lp + '</td>' );
	        },
	      })
				.done( function( response, textStatus, jqXHR ) {
					if ( true === response.success) {
						var notice = 'success';
	          var color  = '#46b450';
					} else {
						var notice = 'error';
						var color = '#DC3232';
					}
					$( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + response.data.message + '</td>' ).fadeOut(3000);
					$( '#wpt-col-actions-' + locale + '-' + slug ).html(
            deactivate_button( slug, name, locale, type ) +
            delete_button( slug, name, locale, type )
          );
				})

     });
   });

   // Premium Check License
   $( "[id^=wpt-col-actions-]" ).on( "click", "[id^=check-license-]", function(e) {
     e.preventDefault();

     var slug    = $( this ).attr( 'data-slug' );
     var name    = $( this ).attr( 'data-name' );
     var type    = $( this ).attr( 'data-type' );
     var locale  = $( this ).attr( 'data-locale' );
     var license = $( '#wpt-license-' + locale + '-' + slug ).val();

     $.ajax({
       type: "POST",
       url : wpt_license_ajax.ajaxurl,
       data: {
         'action'  : 'checkLicense',
         'nonce'   : wpt_license_ajax.nonce,
         'slug'    : slug,
         'type'    : type,
         'name'    : name,
         'locale'  : locale,
       },
       beforeSend: function(response) {
         $('#wpt-spinner-' + locale + '-' + slug ).addClass( 'is-active' );
       },
     })

     .done( function( response, textStatus, jqXHR ) {
       $( '#wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' );
       $( '#wpt-row-notice-' + locale + '-' + slug ).slideDown( 'slow' );
       $( '#wpt-col-actions-' + locale + '-' + slug ).html( spinner( slug, locale, notice ) );

       if ( 'valid' == response.data.api.license && 0 != response.data.api.activations_left ) {
         var notice = 'success';
         $( '#wpt-col-actions-' + locale + '-' + slug ).append( activate_button( slug, name, locale, type ) );
         $( 'check-license-' + slug ).hide();
         $( 'wpt-renew-' + locale + '-' + slug ).hide();
         $( 'wpt-upgrade-' + locale + '-' + slug ).hide();
       } else {
         var notice = 'error';
         if( 'expired' == response.data.api.license || 'expired' == response.data.api.error ) {
           $( '#wpt-col-actions-' + locale + '-' + slug ).append(
             renew_link( slug, locale, license ) +
             check_button( slug, name, locale, type )
           );
         }
         if ( 0 < response.data.api.activations_left ) {
           $( '#wpt-col-actions-' + locale + '-' + slug ).append(
             upgrade_link( slug, locale, license, response.data.api ) +
             check_button( slug, name, locale, type )
           );
         }
       }

       $( '#wpt-row-notice-' + locale + '-' + slug + ' td' ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + response.data.message + '</td>' ).fadeOut(1000);
       $( '#wpt-col-actions-' + locale + '-' + slug ).append( delete_button( slug, name, locale, type ) );

     });
   });

   // Premium Deactivate License
   $( "[id^=wpt-col-actions-]" ).on( "click", "[id^=deactivate-license-]", function(e) {
     e.preventDefault();

     var slug       = $( this ).attr( 'data-slug' );
     var name       = $( this ).attr( 'data-name' );
     var type       = $( this ).attr( 'data-type' );
     var locale     = $( this ).attr( 'data-locale' );
     var license    = $( '#wpt-license-' + locale + '-' + slug ).val();
     var notice     = 'info';
     var color      = '#00A0D2';

     $.ajax({
       type: "POST",
       url : wpt_license_ajax.ajaxurl,
       data: {
         'action'  : 'deactivateLicense',
         'nonce'   : wpt_license_ajax.nonce,
         'slug'    : slug,
         'type'    : type,
         'locale'  : locale,
         'license' : license,
       },
       beforeSend: function(response) {
         $('#wpt-spinner-' + locale + '-' + slug ).addClass( 'is-active' );
         $( '#wpt-row-notice-' + locale + '-' + slug )
           .show()
           .html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.deactivating + '</td>' );
       },
     })

     .done( function( response, textStatus, jqXHR ) {

       if ( true === response.success ) {
         var notice = 'success';
         var color  = '#46b450';
				 $( '#wpt-col-activations-' + locale + '-' + slug ).html('');
				 $( '#wpt-col-status-' + locale + '-' + slug ).html('');
       } else {
         var notice = 'error';
         var color = '#DC3232';
       }
       $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + response.data.message + '</td>' ).fadeOut(1000);
       $( '#wpt-col-actions-' + locale + '-' + slug ).html(
         activate_button( slug, name, locale, type ) +
         delete_button( slug, name, locale, type )
       );

     });
   });


  $( "[id^=wpt-readme-]" ).on( "click", function(e) {
    e.preventDefault();

    var slug    = $( this ).attr( 'data-slug' );
    var locale  = $( this ).attr( 'data-locale' );

    $( '#readme-' + locale + '-' + slug ).toggle();

  });

});
