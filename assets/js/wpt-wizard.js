jQuery( function( $ ) {

	var migrationsActions = $( ".wpt-migrate-actions" ).length;
	console.log(migrationsActions);

	// Premium Spinner
   var spinner = function ( slug, locale, type ) {
     html = '<span id="wpt-spinner-' + locale + '-'+ slug + '" class="wpt-spinner is-' + type + ' alignright"></span>';
     return html;
   }

   // Premium Button Delete Plugin
   var delete_plugin_button = function() {
     html = '<button type="button" class="wpt-button danger" title="' + wpt_license_ajax.i18n.btn.deleted_plugin + '" id="delete-plugin"><span class="dashicons dashicons-dismiss"></span><span class="screen-reader-text">' + wpt_license_ajax.i18n.btn.deleted_plugin + '</span></button>';
     return html;
   };

   // Premium Button Activate
   var activate_button_wizard = function( slug, name, locale, type, license ) {
     html = '<button type="button" class="wpt-button" data-type="'+ type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" data-license="' + license + '" title="' + wpt_license_ajax.i18n.btn.activate + '" id="activate-license-wizard-' + slug + '"><span class="dashicons dashicons-controls-play"></span> ' + wpt_license_ajax.i18n.btn.activate + '</button>';
     return html;
   };

   // Premium Button Activate
   var configure_button_wizard = function( ) {
     html = '<button type="button" class="wpt-button" id="wpt-wizard-config">' + wpt_license_ajax.i18n.btn.configure + '</button>';
     return html;
   };

   // Premium Button Install
   var install_button = function( slug, name, locale, type ) {
     html = '<button type=button class="wpt-button wpt-install-package" data-type="' + type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" ><span class="dashicons dashicons-download"></span> ' + wpt_license_ajax.i18n.btn.download + '</button>';
     return html;
   }

   // Premium Button Next
   var next_step_button = function() {
     html = '<button type=button id="wpt-wizard-start" class="wpt-button primary">' + wpt_license_ajax.i18n.btn.next + '</button>';
     return html;
   }

   // Defaut notices values
   var notice  = 'info';
   var color   = '#00A0D2';


	$( "[id^=wpt-migrate-]" ).on( "click", function(e) {

		e.preventDefault();

		var name       = $( this ).attr( 'data-name' );
		var slug       = $( this ).attr( 'data-slug' );
		var locale     = $( this ).attr( 'data-locale' );
		var type       = $( this ).attr( 'data-type' );
		var license    = $( this ).attr( 'data-license' );
		var pluginkey  = $( this ).attr( 'data-key' );

		$.ajax({
			type: "POST",
			url : wpt_license_ajax.ajaxurl,
			data: {
				'action'  : 'saveLicense',
				'nonce'   : wpt_license_ajax.nonce,
				'name'    : name,
				'slug'    : slug,
				'type'    : type,
				'locale'  : locale,
				'license' : license,
			},
			beforeSend: function(response) {
				$( '#wpt-spinner-' + locale + '-' + slug ).addClass( 'is-active' );
				$( '#wpt-row-notice-' + locale + '-' + slug )
					.show()
					.html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.saving + '</td>' )
			},
		})

		.done( function( response, textStatus, jqXHR ) {

			if ( true === response.success ) {
				var notice = 'success';
				var color  = '#46b450';
			} else {
				var notice = 'error';
				var color = '#DC3232';
			}

			$( '#wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' ).addClass( 'is-' + notice );
			$( '#wpt-row-notice-' + locale + '-' + slug )
				.addClass( 'is-' + notice )
				.html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + response.data.message + '</td>' );
		})
		.then( function( response, textStatus, jqXHR ) {
		$.ajax({
			type: "POST",
			url : wpt_license_ajax.ajaxurl,
			data: {
				'action'  : 'activateLicense',
				'nonce'   : wpt_license_ajax.nonce,
				'slug'    : slug,
				'type'    : type,
				'locale'  : locale,
				'license' : license,
			},
			beforeSend: function(response1) {
				$( '#wpt-spinner-' + locale + '-' + slug ).addClass( 'is-active' );
				$( '#wpt-row-license-' + locale + '-' + slug + ' .column-ajax' ).addClass( 'is-' + notice );
				$( '#wpt-row-notice-' + locale + '-' + slug )
					.show()
					.html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.check_license + '</td>' );
			},
		})

		.done( function( response1, textStatus, jqXHR ) {

			if ( 'valid' == response1.data.api.license ) {
			 var license_limit = ( 'unlimited' == response1.data.api.activations_left ) ? wpt_license_ajax.i18n.status.unlimited : response1.data.api.license_limit;

			 $( '#wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' ).addClass( 'is-success' );
			 $( '#wpt-col-status-' + locale + '-' + slug ).html( '<p>' + wpt_license_ajax.i18n.status.activated + '</p>' );
			 $( '#wpt-col-activations-' + locale + '-' + slug ).html( '<p>' + response1.data.api.site_count + '/' + license_limit + '</p>' );

			} else {

				var notice = 'error';
				var color = '#DC3232';

				if ( 'expired' == response.data.api.license || 'expired' == response1.data.api.error ) {
					$( '#wpt-col-actions-' + locale + '-' + slug ).append(
						renew_link( slug, locale, license )
					);
				}
				if ( 'no_activations_left' == response1.data.api.error ) {

					var upgrade_id = parseInt( response1.data.api.price_id ) + 1;

					if ( upgrade_id <= wpt_license_ajax.products[locale][slug].pricing ) {
						$( '#wpt-col-actions-' + locale + '-' + slug ).append( upgrade_link( slug, locale, license, response1.data.api, upgrade_id ) );
					}
				}

			}

			$( '#wpt-col-actions-' + locale + '-' + slug ).remove( '#activate-license-' + slug );
			$( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + response1.data.message + '</td>' );
			if( 'error' == notice ) {
				return;
			}
		})
		.then( function( response1, textStatus, jqXHR ) {
			if( false == response.success ) {
				return;
			}
			var notice = 'info';
			var color  = '#00A0D2';

			$.ajax({
				type: "POST",
				url: wpt_license_ajax.ajaxurl,
				data: {
					'action': 'clearCacheUpdate',
					'nonce'  : wpt_license_ajax.nonce,
					'type'   : type,
				},
				beforeSend: function(response2) {
						$( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.check_update + '</td>' );
				}
			})

			.done( function( response2, textStatus, jqXHR ) {
					$( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + response2.data.message + '</td>' );
			})
			.then( function( response2, textStatus, jqXHR ) {
				if( false == response.success ) {
					return;
				}
				var notice = 'info';
				var color  = '#00A0D2';

				$.ajax({
					type: "POST",
					url: wpt_license_ajax.ajaxurl,
					data: {
						'action': 'upgradeTranslation',
						'nonce'  : wpt_license_ajax.update_nonce,
						'slug'   : slug,
						'type'   : type,
						'locale' : locale,
					},
					beforeSend: function(response3) {
							$( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.download_lp + '</td>' );
					}
				})

				.done( function( response3, textStatus, jqXHR ) {
					var notice = 'success';
					var color  = '#46b450';
					$('#wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' ).addClass( 'is-success' );
					$( '#wpt-row-license-' + locale + '-' + slug + ' .column-ajax' ).removeClass('is-info').addClass( 'is-' + notice );
					$( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.lp_installed + '</td>' );
				})
				.then( function( response1, textStatus, jqXHR ) {


					$.ajax({
						type: "POST",
						url: wpt_license_ajax.ajaxurl,
						data: {
							'action' : 'deletePlugin',
							'nonce'  : wpt_license_ajax.update_nonce,
							'slug'   : slug,
							'locale' : locale,
							'plugin' : pluginkey
						},
						beforeSend: function(response4) {
								$( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.deleting_plugin + '</td>' );
						}
					})

					.done( function( response4, textStatus, jqXHR ) {
						var notice = 'success';
						var color  = '#46b450';
						$('#wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' ).addClass( 'is-success' );
						$( '#wpt-row-license-' + locale + '-' + slug + ' .column-ajax' ).removeClass('is-info').addClass( 'is-' + notice );
						$( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.deleted_plugin + '</td>' );
						$( '#wpt-migrate-'  + slug + '-' + locale ).fadeOut(1000).remove();
						console.log( $( ".wpt-migrate-actions" ).length );
						if ( 0 == $( ".wpt-migrate-actions" ).length ) {

							location.reload();
						}

					});
				});
			})
		});
		});
	});

	$( "[id^=wpt-delete-plugin-]" ).on( "click", function(e) {
		e.preventDefault();

		var pluginkey  = $( this ).attr( 'data-key' );
		var slug    = $( this ).attr( 'data-slug' );
		var locale  = $( this ).attr( 'data-locale' );
		var notice     = 'info';
		var color      = '#00A0D2';

		$.ajax({
			type: "POST",
			url: wpt_license_ajax.ajaxurl,
			data: {
				'action' : 'deletePlugin',
				'nonce'  : wpt_license_ajax.update_nonce,
				'slug'   : slug,
				'locale' : locale,
				'plugin' : pluginkey
			},
			beforeSend: function(response) {
					$( '#wpt-row-notice-' + locale + '-' + slug ).show().html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.deleting_plugin + '</td>' );
			}
		})

		.done( function( response, textStatus, jqXHR ) {
			var notice = 'success';
			var color  = '#46b450';
			$(' #wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' ).addClass( 'is-success' );
			$( '#wpt-row-license-' + locale + '-' + slug ).removeClass('is-info').addClass( 'is-' + notice );
			$( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.deleted_plugin + '</td>' );
			$( '#wpt-delete-plugin-' + locale + '-' + slug ).fadeOut(1000).remove();
			console.log( $( ".wpt-migrate-actions" ).length );
			if ( 0 == $( ".wpt-migrate-actions" ).length ) {

				location.reload();
			}
		});

	});

});
