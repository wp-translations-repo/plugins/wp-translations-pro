jQuery( function( $ ) {

  $('#update-translations-table').DataTable({
       "pagingType"  : "simple",
       //"lengthChange": false,
       //"searching"   : false,
       "order"       : [[ 1, "asc" ]],
       "columnDefs": [ {
          "targets": 'no-sort',
          "orderable": false,
        } ],
       "language"    : {
        "sProcessing"    : wpt_update_core.i18n.datatable.sProcessing,
        "sSearch"        : wpt_update_core.i18n.datatable.sSearch,
        "sLengthMenu"    : wpt_update_core.i18n.datatable.sLengthMenu,
        "sInfo"          : wpt_update_core.i18n.datatable.sInfo,
        "sInfoEmpty"     : wpt_update_core.i18n.datatable.sInfoEmpty,
        "sInfoFiltered"  : wpt_update_core.i18n.datatable.sInfoFiltered,
        "sLoadingRecords": wpt_update_core.i18n.datatable.sLoadingRecords,
        "sZeroRecords"   : wpt_update_core.i18n.datatable.sZeroRecords,
        "sEmptyTable"    : wpt_update_core.i18n.datatable.sEmptyTable,
        "oPaginate"      : {
            "sFirst"   : wpt_update_core.i18n.datatable.sFirst,
            "sPrevious": wpt_update_core.i18n.datatable.sPrevious,
            "sNext"    : wpt_update_core.i18n.datatable.sNext,
            "sLast"    : wpt_update_core.i18n.datatable.sLast,
        },
        "oAria": {
            "sSortAscending" :  wpt_update_core.i18n.datatable.sSortAscending,
            "sSortDescending": wpt_update_core.i18n.datatable.sSortDescending,
        }
      }
   });

  var spinner = function ( slug, locale, type ) {
    html = '<span id="wpt-spinner-' + locale + '-'+ slug + '" class="wpt-spinner is-' + type + ' alignright"></span>';
    return html;
  }

  $( 'form[name="upgrade-translations"] .button' ).on( "click", function(e) {
    e.preventDefault();

    var $updatesQueue = $('.translations input[type=checkbox]:checked');
    var index=-1;
    var notice  = 'info';
    var color   = '#00A0D2';

    function doNextUpdate() {

      if ( ++index >= $updatesQueue.length ) {
        $( '#update-translations-table input[type=checkbox]' ).prop('checked', false);
        return;
      }

      var update = $updatesQueue.eq(index);
      var slug   = update.val();
      var type   = update.attr( 'data-type' );
      var locale = update.attr( 'data-locale' );

      $.ajax({
        type: "POST",
        url: wpt_update_core.ajaxurl,
        data: {
          'action': 'upgradeTranslation',
          'nonce' : wpt_update_core.nonce,
          'slug'  : slug,
          'type'  : type,
          'locale': locale,
        },
        beforeSend: function(response) {
          $( '<tr id="wpt-row-notice-' + locale + '-' + slug + '"><td colspan="8" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_update_core.i18n.messages.download_lp + '</td></tr>').insertAfter( $( '#wpt-license-row-' + locale + '-' + slug ) );
        },
      })

      .done( function( response, textStatus, jqXHR ) {
        var notice = 'success';
        var color  = '#46b450';
        $( '#wpt-license-row-' + locale + '-' + slug ).fadeOut();
        $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="8" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_update_core.i18n.messages.lp_installed + '</td>' ).fadeOut();
        $( '#wpt-row-notice-' + locale + '-' + slug ).remove();
        var availables_updates = $( '.wp-translations-to-update' ).length;
        if( availables_updates == 0 ) {
          $( '#update-translations-table' ).slideUp("slow").remove();
          $( 'form[name="upgrade-translations"] .button' ).remove();
          $( 'form[name="upgrade-translations"] p:first-of-type' ).html( wpt_update_core.all_updated_message );
        }
        doNextUpdate();
      });

    }

    doNextUpdate();

  });

  $( '[id^="wp-translations-update-"]' ).on( "click", function(e) {
    e.preventDefault();

    var slug   = $( this ).attr( 'data-slug' );
    var type   = $( this ).attr( 'data-type' );
    var locale = $( this ).attr( 'data-locale' );
    var notice  = 'info';
    var color   = '#00A0D2';

    $.ajax({
      type: "POST",
      url: wpt_update_core.ajaxurl,
      data: {
        'action': 'upgradeTranslation',
        'nonce'  : wpt_update_core.nonce,
        'slug'   : slug,
        'type'   : type,
        'locale' : locale,
      },
      beforeSend: function(response) {
        $( '<tr id="wpt-row-notice-' + locale + '-' + slug + '"><td colspan="8" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_update_core.i18n.messages.download_lp + '</td></tr>').insertAfter( $( '#wpt-license-row-' + locale + '-' + slug ) );
      },
    })

    .done( function( response, textStatus, jqXHR ) {

      var notice = 'success';
      var color  = '#46b450';
      $( '#wpt-license-row-' + locale + '-' + slug ).fadeOut();
      $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="8" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_update_core.i18n.messages.lp_installed + '</td>' ).fadeOut();
      $( '#wpt-row-notice-' + locale + '-' + slug ).remove();
      var availables_updates = $( '.wp-translations-to-update' ).length;
      if ( availables_updates == 0 ) {
        $( '#update-translations-table' ).slideUp().remove();
        $( 'form[name="upgrade-translations"] .button' ).remove();
        $( 'form[name="upgrade-translations"] p:first-of-type' ).html( wpt_update_core.all_updated_message );
      }
    });


  });

  $( "#translations-select-all" ).on( 'change',function() {
    $(".wpt-translations-checked").prop('checked',$(this).is(":checked"));
  });

});
