<?php
/**
 * Wizard
 *
 * @package     WP_Translations_Pro
 * @subpackage  templates/admin
 * @since      1.1.6
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
use WP_Translations_Pro\APIs\EDD_Api;
use WP_Translations_Pro\WordPress\Helpers\ProductHelper;
use WP_Translations_Pro\WordPress\Helpers\WizardHelper;

$errors = get_site_option( 'wpt_pro_pending_migration' );
$locale = get_user_locale();
?>

<div class="wrap wpt-page wpt-page-wizard">
	<img src="<?php echo WPTPRO_PLUGIN_URL .'assets/img/fxbenard-logo.png'; ?>" style="max-height: 64px" />
	<div class="wpt-wizard-step card">
		<div id="wpt-wizard-step-content">
      <h2><?php esc_html_e( 'Migration Wizard', 'wp-translations-pro' );  ?></h2>
			<p><?php esc_html_e( 'The migration of your old products is necessary before continuing.', 'wp-translations-pro' ); ?></p>

			<table class="wpt-table wpt-plugins-conflict" style="width: 100%">
				<thead>
					<tr>
						<th><?php esc_html_e( 'Plugins', 'wp-translations-pro' ); ?></th>
						<th><?php esc_html_e( 'Licenses', 'wp-translations-pro' ); ?></th>
						<th class="column-action"><?php esc_html_e( 'Actions', 'wp-translations-pro' ); ?></th>
					</tr>
				</thead>
			<?php
				foreach( $errors as $key => $error ) :
			?>

				<tr id="wpt-row-license-<?php echo esc_attr( $key ); ?>-<?php echo get_user_locale(); ?>">
					<td><strong><?php echo esc_html( $error['name'] ); ?></strong></td>
					<td>
						<?php if (  false !== get_site_option( $error['license'] ) ) : ?>
							<?php echo get_site_option( $error['license'] ); ?>
						<?php endif; ?>
					</td>
					<td id="wpt-col-actions-<?php echo get_user_locale(); ?>-<?php echo esc_attr( $key ); ?>" class="column-action">
							<?php if (  get_site_option( $error['license'] ) ) : ?>
							<button
								id="wpt-migrate-<?php echo esc_attr( $error['product'] ); ?>-<?php echo esc_attr( $locale ); ?>"
								class="wpt-button primary wpt-migrate-actions"
								data-name="<?php echo esc_html( $error['Name'] ); ?>"
								data-type="<?php echo esc_attr(  $error['type'] ); ?>"
								data-key="<?php echo $error['path']; ?>"
								data-slug="<?php echo $error['product']; ?>"
								data-locale="<?php echo $error['lang']; ?>"
								data-license="<?php echo get_site_option( $error['license'] ); ?>"><?php esc_html_e( 'Migrate license', 'wp-translations-pro' ); ?></button>
							<?php else: ?>
								<button class="wpt-button danger wpt-migrate-actions"
												id="wpt-delete-plugin-<?php echo esc_attr( $locale ); ?>-<?php echo $error['product']; ?>"
												data-key="<?php echo $error['path']; ?>"
												data-slug="<?php echo $error['product']; ?>"
												data-locale="<?php echo $locale; ?>"><?php esc_html_e( 'Delete old plugin', 'wp-translations-pro' ); ?></button>
							<?php endif; ?>
						</td>
				</tr>
				<tr id="wpt-row-notice-<?php echo $locale; ?>-<?php echo esc_attr( $key ); ?>" class="wpt-license-notice hidden">

				</tr>
			<?php endforeach; ?>
		</table>

		</div>

		<div class="wpt-wizard-actions">
    </div>
	</div>
</div>
