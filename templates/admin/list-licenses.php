<?php
/**
 * List Licenses
 *
 * @package     WP_Translations_Pro
 * @subpackage  templates/admin
 * @since      1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

use WP_Translations_Pro\WordPress\Helpers\Helper;
use WP_Translations_Pro\WordPress\Helpers\ProductHelper;
use WP_Translations_Pro\WordPress\Helpers\LicenseHelper;
use WP_Translations_Pro\WordPress\Helpers\TranslationHelper;
use WP_Translations_Pro\WordPress\Helpers\ReadmeHelper;

$localProducts = ProductHelper::localProducts();
$storesData    = ProductHelper::STORES;
$options       = Helper::getOptions();
$updates       = wp_get_translation_updates();
$currentLocale = get_user_locale();
$hasMulitpleTabs = 'wpt-tab-alone';
?>

<div class="wpt-box postbox">

		<h2>
			<span>
				<span class="dashicons dashicons-admin-network"></span>
					<?php esc_html_e( 'Licenses', 'wp-translations-pro' ); ?>
				</span>
			</h2>


    <div class="js-tabs wpt-settings-tabs">
      <ul class="js-tablist" data-tabs-prefix-class="wpt-settings">
        <?php foreach ( $localProducts as $locale => $store ) :
					if ( 1 < count( $localProducts ) ) :
          $activeTab = ( $locale == $currentLocale ) ? 'data-selected="1"' : ''; ?>
        <li class="js-tablist__item">
          <a href="#store-<?php echo $locale; ?>" id="label_store-<?php echo $locale; ?>" class="js-tablist__link" <?php echo $activeTab; ?>><span class="dashicons dashicons-translation"></span> <?php echo $locale; ?> - <?php esc_html_e('Available translations', 'wp-translations-pro' ); ?></a>
        </li>
        <?php
					endif;
				endforeach; ?>
      </ul><!-- /end .wpt-tab-nav -->
				<?php if ( empty( $localProducts ) ) : ?>
					<div class="wpt-empty-table">
						<form id="wpt-settings-form" action="" method="POST">
							<input type="hidden" name="wpt-pro-action" value="saveSettings"/>
				      <input type="hidden" name="wp-pro-settings-nonce" value="<?php echo wp_create_nonce( 'wpt_pro_settings_nonce' ); ?>"/>
							<button type="submit" id="wpt-pro-force-product-update" name="wpt_pro_settings[force_products]" class="wpt-button"><?php esc_html_e( 'Update Products', 'wp-translations-pro' ); ?></button>
						</form>
					</div>
				<?php endif; ?>
        <?php	foreach ( $localProducts as $locale => $store ) :	?>

        <div id="store-<?php echo $locale; ?>" class="<?php echo esc_attr( $hasMulitpleTabs ); ?>">
          <table class="wpt-settings-table wpt-license-table">

            <thead>
              <tr>
                <th scope="col" valign="top"><?php esc_html_e( 'Name', 'wp-translations-pro' ); ?></th>
                <th scope="col" valign="top"><?php esc_html_e( 'License Key', 'wp-translations-pro' ); ?></th>
                <th scope="col" valign="top"><?php esc_html_e( 'Activations', 'wp-translations-pro' ); ?></th>
                <th scope="col" valign="top"><?php esc_html_e( 'Status', 'wp-translations-pro' ); ?></th>
                <th scope="col" class="column-actions" valign="top"><?php esc_html_e( 'Actions', 'wp-translations-pro' ); ?></th>
              </tr>
            </thead>

            <tbody>
              <?php foreach ( $store as $key => $product ) :
                $license    = isset( $options['licenses'][ $locale ][ $product['slug'] ] ) ? $options['licenses'][ $locale ][ $product['slug'] ]['license'] : '';
                $infos      = ! empty( $options['licenses'][ $locale ][ $product['slug'] ]['data'] ) ? $options['licenses'][ $locale ][ $product['slug'] ]['data'] : array();
                $type       = $store[ $product['slug'] ]['type'];
                $iconType   = ( 'plugin' == $type ) ? 'dashicons-admin-plugins' : 'dashicons-admin-appearance';
							?>

              <tr id="wpt-row-license-<?php echo $locale; ?>-<?php echo esc_attr( $product['slug'] ); ?>" class="wpt-license-row">
                <td scope="row" valign="top">
                  <label for="wpt-license-<?php echo $locale; ?>-<?php echo esc_attr( $product['slug'] ); ?>"><span class="dashicons <?php echo esc_attr( $iconType ); ?>"></span> <?php echo esc_html( $product['title'] ); ?></label>
                  <?php
                  $modalArgs = array(
                    'button_css_class' => 'wpt-button-link',
                    'button_text'      => '',
                    'button_icon'      => 'dashicons-info',
                    'active_tab'       => 'description'
                  );
                  ReadmeHelper::displayReadmeModal( TranslationHelper::sanitizeTextdomain( $product['slug'] ), $locale, $modalArgs );
                  ?>

                </td>
                <td>
                  <input type="password" class="" id="wpt-license-<?php echo $locale; ?>-<?php echo esc_attr( $product['slug'] ); ?>" value="<?php echo $license; ?>" />
                </td>
                <td id="wpt-col-activations-<?php echo $locale; ?>-<?php echo esc_attr( $product['slug'] ); ?>">
                  <?php  if( LicenseHelper::isValid( $product['slug'], $locale ) ) {
										$limit = ( 'unlimited' == $options['licenses'][ $locale ][ $product['slug'] ]['data']->license_limit ) ? esc_html__( 'unlimited', 'wp-translations-pro' ) : $options['licenses'][ $locale ][ $product['slug'] ]['data']->license_limit;
                    echo $options['licenses'][ $locale ][ $product['slug'] ]['data']->site_count . '/' . $limit;
                  } ?>
                </td>
                <td id="wpt-col-status-<?php echo $locale; ?>-<?php echo esc_attr( $product['slug'] ); ?>">

                  <?php if( LicenseHelper::isSave( $product['slug'], $locale ) ) : ?>
                    <?php echo LicenseHelper::messageStatus( $infos ); ?>
                  <?php endif; ?>
                </td>
                <td id="wpt-col-actions-<?php echo $locale; ?>-<?php echo esc_attr( $product['slug'] ); ?>" class="column-actions">

                  <?php if ( ! LicenseHelper::isSave( $product['slug'], $locale ) ) : ?>

                    <button type="button" class="wpt-button" data-type="<?php echo $type . 's'; ?>" data-locale="<?php echo $locale; ?>" data-slug="<?php echo esc_attr( $product['slug'] ); ?>" data-name="<?php echo $product['title']; ?>" id="save-license-<?php echo esc_attr( $product['slug'] ); ?>"><span class="dashicons dashicons-plus-alt"></span> <?php esc_html_e( 'Save license', 'wp-translations-pro' ); ?></button>
                    <a href="<?php echo esc_url( $product['link'] ); ?>" class="wpt-button" target="_blank"><span class="dashicons dashicons-admin-network"></span> <?php esc_html_e( 'Get a license', 'wp-translations-pro' ); ?></a>

                  <?php else : ?>

                    <!-- Deactivation button -->
                    <?php if ( ! empty( $infos ) && 'valid' == $infos->license ) : ?>
                      <button type="button" class="wpt-button" data-type="<?php echo $type . 's'; ?>" data-locale="<?php echo $locale; ?>" data-slug="<?php echo esc_attr( $product['slug'] ); ?>" data-name="<?php echo $product['title']; ?>" id="deactivate-license-<?php echo esc_attr( $product['slug'] ); ?>"><span class="dashicons dashicons-controls-pause"></span> <?php esc_html_e( 'Deactivate', 'wp-translations-pro' ); ?></button>
                    <?php endif; ?>


                    <?php if ( ! empty( $infos ) && 'valid' != $infos->license ) :

                      if ( isset( $infos->error ) && isset( $infos->price_id ) ) :
                        $upgrade_id = $infos->price_id + 1;
                        if ( 'no_activations_left' == $infos->error && $upgrade_id <= $store[ $product['slug'] ]['pricing'] ) : ?>

													<!-- Upgrade link -->
                          <a href="<?php echo $storesData[ $locale ]['url']; ?><?php echo $storesData[ $locale ]['checkout']; ?>?edd_action=sl_license_upgrade&license_id=<?php echo absint( $infos->license_id ); ?>&upgrade_id=<?php echo absint( $upgrade_id ); ?>" class="wpt-button" target="_blank"><span class="dashicons dashicons-unlock"></span> <?php esc_html_e( 'Upgrade', 'wp-translations-pro' ); ?></a>
                          <button type="button" class="wpt-button" data-type="<?php echo $type . 's'; ?>" data-locale="<?php echo $locale; ?>" data-slug="<?php echo esc_attr( $product['slug'] ); ?>" data-name="<?php echo $product['title']; ?>" id="activate-license-<?php echo esc_attr( $product['slug'] ); ?>"><span class="dashicons dashicons-controls-play"></span> <?php esc_html_e( 'Activate', 'wp-translations-pro' ); ?></button>
                        <?php endif; ?>
                      <?php endif; ?>


                      <!-- Renew link -->
                      <?php if ( isset( $infos->error ) && 'expired' == $infos->error || 'expired' == $infos->license ) : ?>
                        <a href="<?php echo $storesData[ $locale ]['url']; ?><?php echo $storesData[ $locale ]['checkout']; ?>?edd_license_key=<?php echo $license; ?>&download_id=<?php echo absint( $product['id'] ); ?>" id="wpt-renew-<?php echo $locale; ?>-<?php echo $product['slug']; ?>" class="wpt-button" target="_blank"><span class="dashicons dashicons-image-rotate"></span> <?php esc_html_e( 'Renew', 'wp-translations-pro' ); ?></a>
                        <button type="button" class="wpt-button" data-locale="<?php echo $locale; ?>" data-type="<?php echo $type . 's'; ?>" data-slug="<?php echo esc_attr( $product['slug'] ); ?>" data-name="<?php echo $product['title']; ?>" id="activate-license-<?php echo esc_attr( $product['slug'] ); ?>"><span class="dashicons dashicons-controls-play"></span> <?php esc_html_e( 'Activate', 'wp-translations-pro' ); ?></button>
                      <?php endif; ?>
                    <?php endif; ?>

                    <!-- Activation button -->
                    <?php if ( empty( $infos ) || 'site_inactive' == $infos->license  ) : ?>
                      <button type="button" class="wpt-button" data-locale="<?php echo $locale; ?>" data-type="<?php echo $type . 's'; ?>" data-slug="<?php echo esc_attr( $product['slug'] ); ?>" data-name="<?php echo $product['title']; ?>" id="activate-license-<?php echo esc_attr( $product['slug'] ); ?>"><span class="dashicons dashicons-controls-play"></span> <?php esc_html_e( 'Activate', 'wp-translations-pro' ); ?></button>
                    <?php endif; ?>

                    <button type="button" class="wpt-button danger" title="<?php esc_html_e( 'Delete license', 'wp-translations-pro' ); ?>" data-type="<?php echo $type . 's'; ?>" data-locale="<?php echo $locale; ?>" data-slug="<?php echo esc_attr( $product['slug'] ); ?>" data-name="<?php echo $product['title']; ?>" id="delete-license-<?php echo esc_attr( $product['slug'] ); ?>"><span class="dashicons dashicons-dismiss"></span><span class="screen-reader-text"><?php esc_html_e( 'Delete license', 'wp-translations-pro' ); ?></span></button>
                  <?php endif; ?>
                </td>

              </tr>

              <tr id="wpt-row-notice-<?php echo $locale; ?>-<?php echo esc_attr( $product['slug'] ); ?>" class="wpt-license-notice hidden">

              </tr>
            <?php endforeach; ?>
            </tbody>

          </table>
        </div><!-- /end .js-tabcontent -->
        <?php endforeach; ?>

    </div><!-- /end .wpt-settings-tabs -->

</div><!-- /end .wpt-box -->

<?php if ( false !== (bool) $options['settings']['debug'] ) : ?>
<div class="postbox">
  <h2>Debug</h2>
  <div class="inside">

    <?php
    foreach ( $options['licenses'] as $locale => $products ) {
      foreach ( $products as $slug => $license ) {

        $transient = get_site_transient( 'wpt_license_' . $locale . '_' . $slug );
        $licenses[ $locale ][ $slug ]              = $license;
        $licenses[ $locale ][ $slug ]['transient'] = $transient;
      }
    }

    if ( isset( $licenses ) ) : ?>
    <div class="wpt-debug">
      <h3><?php esc_html_e( 'Licenses', 'wp-translations-pro' ); ?></h3>
      <?php Helper::displayArrayRecursively( $licenses ); ?>
    </div>
    <?php endif; ?>

    <?php if ( isset( $localProducts ) ) : ?>
    <div class="wpt-debug">
      <h3><?php esc_html_e( 'Products', 'wp-translations-pro' ); ?></h3>
      <?php Helper::displayArrayRecursively( $localProducts ); ?>
    </div>
    <?php endif; ?>

  </div>
</div>
<?php endif;
