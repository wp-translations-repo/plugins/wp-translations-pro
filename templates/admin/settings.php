<?php
/**
 * Settings
 *
 * @package     WP_Translations_Pro
 * @subpackage  templates/admin
 * @since      1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

use WP_Translations_Pro\WordPress\Helpers\Helper;

$options         = Helper::getOptions();
$coreUpdates     = ! empty( $options['settings']['core_updates'] ) ? (bool) $options['settings']['core_updates'] : false;
$pluginsUpdates  = ! empty( $options['settings']['plugins_updates'] ) ? (bool) $options['settings']['plugins_updates'] : false;
$themesUpdates   = ! empty( $options['settings']['themes_updates'] ) ? (bool) $options['settings']['themes_updates'] : false;
$bubbleCount     = ! empty( $options['settings']['bubble_count'] ) ? (bool) $options['settings']['bubble_count'] : false;
$pageHook        = ! empty( $options['settings']['page_hook'] ) ? $options['settings']['page_hook'] : false;
$debug           = ! empty( $options['settings']['debug'] ) ? $options['settings']['debug'] : false;
?>

<div class="wpt-box postbox">
  <form id="wpt-settings-form" action="" method="POST">
    <h2><span><span class="dashicons dashicons-admin-settings"></span> <?php esc_html_e( 'Settings', 'wp-translations-pro' ); ?></span></h2>

      <div class="js-tabs wpt-settings-tabs">
      <ul class="js-tablist" data-tabs-prefix-class="wpt-settings">
          <li class="js-tablist__item">
            <a href="#settings-ui" id="label_settings-ui" class="js-tablist__link"><span class="dashicons dashicons-welcome-view-site"></span> <?php esc_html_e( 'UI Integration', 'wp-translations-pro' ); ?></a>
          </li>
          <li class="js-tablist__item">
            <a href="#advanced" id="label_advanced" class="js-tablist__link"><span class="dashicons dashicons-admin-generic"></span> <?php esc_html_e( 'Advanced', 'wp-translations-pro' ); ?></a>
          </li>
        </ul><!-- /end .js-tablist -->

        <div id="settings-ui" class="js-tabcontent">
          <table class="wpt-settings-table">

            <thead>
              <tr>
                <th scope="col" valign="top"><?php esc_html_e( 'Options', 'wp-translations-pro' ); ?></th>
                <th scope="col" valign="top"><?php esc_html_e( 'Description', 'wp-translations-pro' ); ?></th>
                <th scope="col" class="column-actions" style="width:350px;" valign="top"><?php esc_html_e( 'Actions', 'wp-translations-pro' ); ?></th>
              </tr>
            </thead>

            <tbody>
              <tr class="wpt-license-row">
                <td scope="row" valign="top">
                  <label for="wpt-pro-core-updates"><?php esc_html_e( 'Core updates', 'wp-translations-pro' ); ?></label>
                  <i class="dashicons dashicons-arrow-right"></i>
                </td>
                <td>
                  <span class="description"><?php esc_html_e( 'Display translations updates details in core\'s update page.', 'wp-translations-pro' ); ?></span>
                </td>
                <td class="column-actions">
                  <input name="wpt_pro_settings[core_updates]" id="wpt-pro-core-updates" class="switch" type="checkbox" value="1" <?php checked( true, $coreUpdates ); ?>/>
                </td>
              </tr>
              <tr class="wpt-license-row">
                <td scope="row" valign="top">
                  <label for="wpt-pro-plugins-updates"><?php esc_html_e( 'Plugins updates', 'wp-translations-pro' ); ?></label>
                  <i class="dashicons dashicons-arrow-right"></i>
                </td>
                <td>
                  <span class="description"><?php esc_html_e( 'Display translations updates details in plugin\'s update page.', 'wp-translations-pro' ); ?></span>
                </td>
                <td class="column-actions">
                  <input name="wpt_pro_settings[plugins_updates]" id="wpt-pro-plugins-updates" class="switch" type="checkbox" value="1" <?php checked( true, $pluginsUpdates ); ?>/>
                </td>
              </tr>
              <tr class="wpt-license-row">
                <td scope="row" valign="top">
                  <label for="wpt-pro-themes-updates"><?php esc_html_e( 'Themes updates', 'wp-translations-pro' ); ?></label>
                  <i class="dashicons dashicons-arrow-right"></i>
                </td>
                <td>
                  <span class="description"><?php esc_html_e( 'Display translations updates details in theme\'s update page.', 'wp-translations-pro' ); ?></span>
                </td>
                <td class="column-actions">
                  <input name="wpt_pro_settings[themes_updates]" id="wpt-pro-themes-updates" class="switch" type="checkbox" value="1" <?php checked( true, $themesUpdates ); ?>/>
                </td>
              </tr>
              <tr class="wpt-license-row">
                <td scope="row" valign="top">
                  <label for="wpt-pro-bubble-count"><?php esc_html_e( 'Bubble count', 'wp-translations-pro' ); ?></label>
                  <i class="dashicons dashicons-arrow-right"></i>
                </td>
                <td>
                  <span class="description"><?php esc_html_e( 'Display translations updates in the bubble menu bar.', 'wp-translations-pro' ); ?></span>
                </td>
                <td class="column-actions">
                  <input name="wpt_pro_settings[bubble_count]" id="wpt-pro-bubble-count" class="switch" type="checkbox" value="1" <?php checked( true, $bubbleCount ); ?>/>
                </td>
              </tr>
              <tr class="wpt-license-row">
                <td scope="row" valign="top">
                  <label for="wpt-pro-page-hook"><?php esc_html_e( 'Admin page position', 'wp-translations-pro' ); ?></label>
                  <i class="dashicons dashicons-arrow-right"></i>
                </td>
                <td>

                </td>
                <td class="column-actions">
                  <select name="wpt_pro_settings[page_hook]" id="wpt-pro-page-hook">
                    <option value="menu" <?php selected( $pageHook, 'menu' ); ?>><?php esc_html_e( 'Dedicated page', 'wp-translations-pro' ); ?></option>
                    <option value="options" <?php selected( $pageHook, 'options' ); ?>><?php esc_html_e( 'Settings subpage', 'wp-translations-pro' ); ?></option>
                  </select>
                </td>
              </tr>
            </tbody>
          </table>
        </div><!-- /end #settings-ui -->

        <div id="advanced" class="js-tabcontent" aria-hidden="true">
          <table class="wpt-settings-table">

            <thead>
              <tr>
                <th scope="col" valign="top"><?php esc_html_e( 'Options', 'wp-translations-pro' ); ?></th>
                <th scope="col" valign="top"><?php esc_html_e( 'Description', 'wp-translations-pro' ); ?></th>
                <th scope="col" class="column-actions" style="width:350px;" valign="top"><?php esc_html_e( 'Actions', 'wp-translations-pro' ); ?></th>
              </tr>
            </thead>

            <tbody>

              <tr class="wpt-license-row">
                <td scope="row" valign="top">
                  <label for="wpt-pro-debug"><?php esc_html_e( 'Debug Mode', 'wp-translations-pro' ); ?></label>
                  <i class="dashicons dashicons-arrow-right"></i>
                </td>
                <td>
                  <span class="description"><?php esc_html_e( 'Toggle debug mode, it may affect performance.', 'wp-translations-pro' ); ?></span>
                </td>
                <td class="column-actions">
                  <input name="wpt_pro_settings[debug]" id="wpt-pro-debug" class="switch" type="checkbox" value="1" <?php checked( true, $debug ); ?>/>
                </td>
              </tr>

              <tr class="wpt-license-row">
                <td scope="row" valign="top">
                  <label for="wpt-pro-force-product-update"><?php esc_html_e( 'Products Update', 'wp-translations-pro' ); ?></label>
                  <i class="dashicons dashicons-arrow-right"></i>
                </td>
                <td>
                  <span class="description"><?php esc_html_e( 'Force products update from stores.', 'wp-translations-pro' ); ?></span>
                </td>
                <td class="column-actions">
                  <button type="submit" id="wpt-pro-force-product-update" name="wpt_pro_settings[force_products]" class="wpt-button"><?php esc_html_e( 'Update Products', 'wp-translations-pro' ); ?></button>
                </td>
              </tr>

              <tr class="wpt-license-row">
                <td scope="row" valign="top">
                  <label for="wpt-pro-force-translations-update"><?php esc_html_e( 'Check Translations Updates', 'wp-translations-pro' ); ?></label>
                  <i class="dashicons dashicons-arrow-right"></i>
                </td>
                <td>
                  <span class="description"><?php esc_html_e( 'Force translations updates from stores.', 'wp-translations-pro' ); ?></span>
                </td>
                <td class="column-actions">
                  <button type="submit" id="wpt-pro-force-translations-update" name="wpt_pro_settings[force_translations]" class="wpt-button"><?php esc_html_e( 'Check for updates', 'wp-translations-pro' ); ?></button>
                </td>
              </tr>

              <tr class="wpt-license-row">
                <td scope="row" valign="top">
                  <label for="wpt-pro-purge-log-update"><?php esc_html_e( 'Purge Logs', 'wp-translations-pro' ); ?></label>
                  <i class="dashicons dashicons-arrow-right"></i>
                </td>
                <td>

                </td>
                <td class="column-actions">
                  <button type="submit" id="wpt-pro-purge-log-update" name="wpt_pro_settings[purge_logs]" class="wpt-button"><?php esc_html_e( 'Purge', 'wp-translations-pro' ); ?></button>
                </td>
              </tr>

            </tbody>
          </table>

        </div><!-- /end #advanced -->

      </div><!-- /end .wpt-licenses-tabs -->
    <div class="wpt-table-footer">
      <input type="hidden" name="wpt-pro-action" value="saveSettings"/>
      <input type="hidden" name="wp-pro-settings-nonce" value="<?php echo wp_create_nonce( 'wpt_pro_settings_nonce' ); ?>"/>
      <input type="submit" value="<?php esc_html_e( 'Save Settings', 'wp-translations-pro' ); ?>" class="button-primary"/>
    </div>
  </form>
</div><!-- /end .wpt-box -->


<?php if ( false !== (bool) $options['settings']['debug'] ) : ?>
<div class="postbox">
  <h2>Debug</h2>
  <div class="inside">

      <?php if ( isset( $options['settings'] ) ) : ?>
      <div class="wpt-debug">
        <h3><?php esc_html_e( 'Settings', 'wp-translations-pro' ); ?></h3>
        <?php Helper::displayArrayRecursively( $options['settings'] ); ?>
      </div>
      <?php endif; ?>

  </div>
</div>
<?php endif; ?>
