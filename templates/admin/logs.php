<?php
/**
 * List Licenses
 *
 * @package     WP_Translations_Pro
 * @subpackage  templates/admin
 * @since      1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

use WP_Translations_Pro\WordPress\Helpers\Helper;

$options    = Helper::getOptions();
$logs = get_site_option( 'wpt_pro_logs' ); ?>

<div class="wpt-box postbox">
	<h2><span><span class="dashicons dashicons-backup"></span> <?php esc_html_e( 'Logs', 'wp-translations-pro' ); ?></span></h2>

		<div class="js-tabs wpt-settings-tabs">
			<ul class="js-tablist" data-tabs-prefix-class="wpt-settings">
				<li class="js-tablist__item">
					<a href="#licenses-log" id="label_licenses-log" class="js-tablist__link"><span class="dashicons dashicons-admin-network"></span> <?php esc_html_e( 'Licenses', 'wp-translations-pro' ); ?></a>
				</li>
				<li class="js-tablist__item">
					<a href="#updates-log" id="label_updates-log" class="js-tablist__link"><span class="dashicons dashicons-update"></span> <?php esc_html_e( 'Updates', 'wp-translations-pro' ); ?></a>
				</li>
			</ul>


				<div id="licenses-log" class="js-tabcontent">
					<table id="table_licenses_logs" class="wpt-settings-table stripe hover">

						<thead>
							<tr>
								<th scope="col" valign="top"><?php esc_html_e( 'Date', 'wp-translations-pro' ); ?></th>
								<th scope="col" valign="top"><?php esc_html_e( 'License Key', 'wp-translations-pro' ); ?></th>
								<th scope="col" valign="top"><?php esc_html_e( 'Action', 'wp-translations-pro' ); ?></th>
								<th scope="col" valign="top"><?php esc_html_e( 'Status', 'wp-translations-pro' ); ?></th>
							</tr>
						</thead>

						<tbody>
							<?php if ( ! empty( $logs['license'] )  ) :
								foreach( $logs['license'] as $date => $log ) : ?>

								<tr class="wpt-license-row">
									<td><?php echo date_i18n( "Y-m-d H:i:s", $log['date'] ); ?></td>
									<td><?php echo $log['license']; ?></td>
									<td><?php echo $log['title']; ?></td>
									<td class="column-actions"><?php echo $log['status']['message']; ?></td>
								</tr>

							<?php endforeach;
							endif; ?>
						</tbody>
					</table>
				</div><!-- /end .js-tabcontent -->
				<div id="updates-log" class="js-tabcontent" aria-hidden="true">
					<table id="table_updates_logs" class="wpt-settings-table stripe hover">

						<thead>
							<tr>
								<th scope="col" valign="top"><?php esc_html_e( 'Date', 'wp-translations-pro' ); ?></th>
								<th scope="col" valign="top"><?php esc_html_e( 'Text Domain', 'wp-translations-pro' ); ?></th>
								<th scope="col" valign="top"><?php esc_html_e( 'Language', 'wp-translations-pro' ); ?></th>
								<th scope="col" valign="top"><?php esc_html_e( 'Version', 'wp-translations-pro' ); ?></th>
								<th scope="col" valign="top"><?php esc_html_e( 'Action', 'wp-translations-pro' ); ?></th>
							</tr>
						</thead>

						<tbody>
							<?php if ( ! empty( $logs['update'] )  ) :
							foreach( $logs['update'] as $date => $log ) :
								$textdomain = substr( $log['textdomain'], 0, strrpos( $log['textdomain'], '-' ) );
								$language   = substr( strrchr( $log['textdomain'], "-" ), 1 );
							?>

								<tr class="wpt-license-row">
									<td><?php echo date_i18n( "Y-m-d H:i:s", $log['date'] ); ?></td>
									<td><?php echo $textdomain; ?></td>
									<td><?php echo $language; ?></td>
									<td><?php echo $log['version']['product']; ?> rev(<?php echo $log['version']['po_revision']; ?>)</td>
									<td><?php echo $log['title']; ?></td>
								</tr>

							<?php endforeach;
							endif; ?>
						</tbody>
					</table>
				</div><!-- /end .js-tabcontent -->

		</div><!-- /end .wpt-settings-tabs -->

</div><!-- /end .postbox -->

<?php if ( false !== (bool) $options['settings']['debug'] ) : ?>
	<div class="postbox">
		<h2>Debug</h2>
		<div class="inside">

			<?php if ( isset( $logs ) ): ?>
			<div class="wpt-debug">
				<h3><?php esc_html_e( 'Logs', 'wp-translations-pro' ); ?></h3>
				<?php Helper::displayArrayRecursively( $logs ); ?>
			</div>
			<?php endif; ?>

		</div>
	</div>
<?php endif; ?>
