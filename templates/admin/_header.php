<?php
/**
 * Partial Admin Header
 *
 * @package     WP_Translations_Pro
 * @subpackage  templates/admin
 * @since      1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

use WP_Translations_Pro\WordPress\Helpers\Helper;

$page = isset( $_GET['wpt-page'] ) ? $_GET['wpt-page'] : false;
?>

<div class="wrap wpt-page">

  <div class="wpt-nav">
    <div class="wp-filter">
      <ul class="filter-links filter-links-left">
				<li>
					<img src="<?php echo WPTPRO_PLUGIN_URL .'assets/img/fxbenard-logo.png'; ?>" style="max-height: 32px" />
				</li>
        <li>
          <a class="<?php if ( 'licenses' == $page || false === $page ) { echo'current'; } ?>" href="<?php echo Helper::adminUrl( 'admin.php?page=wp-translations-pro&wpt-page=licenses' ); ?>"><span class="dashicons dashicons-admin-network"></span> <?php esc_html_e( 'Licenses', 'wp-translations-pro' ); ?></a>
        </li>
        <li>
          <a class="<?php if ( 'logs' == $page ) { echo'current'; } ?>" href="<?php echo Helper::adminUrl( 'admin.php?page=wp-translations-pro&wpt-page=logs' ); ?>"><span class="dashicons dashicons-backup"></span> <?php esc_html_e( 'Logs', 'wp-translations-pro' ); ?></a>
        </li>

      </ul>
      <ul class="filter-links filter-links-right">
				<li>
          <a class="<?php if ( 'settings' == $page ) { echo'current'; } ?>" href="<?php echo Helper::adminUrl( 'admin.php?page=wp-translations-pro&wpt-page=settings' ); ?>"><span class="dashicons dashicons-admin-settings"></span> <?php esc_html_e( 'Settings', 'wp-translations-pro' ); ?></a>
        </li>
      </ul>
    </div>

  </div><!-- /end .wpt-nav -->
<h2 class="screen-reader-text" style="margin:0;">WP-Translations</h2>
