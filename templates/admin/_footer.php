<?php
/**
 * Partial Admin Footer
 *
 * @package     WP_Translations_Pro
 * @subpackage  templates/admin
 * @since      1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

?>

</div> <!-- /end .wrap -->
