<?php

namespace WP_Translations_Pro\APIs;

/**
 * Classe to communicate with the EDD API
 *
 * @author     WP-Translations Team
 * @link       https://wp-translations.pro
 * @since      1.0.0
 *
 * @package    WPT_transifex_Pro
 */

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

/**
 * Class from EDD API
 *
 * @since 1.0.0
 */
abstract class EDD_Api {

	protected static function httpArgs() {

		$args = array(
			'httpversion' => '1.1',
			'method'      => 'GET',
			'timeout'     => 120,
		);

		return $args;
	}

	protected static function getRemoteData( $endpoint, $locale ) {

		$cached = get_site_transient( 'wpt_edd_products_' . $locale );

		if ( false === $cached ) {

			$cached = wp_remote_get( $endpoint, self::httpArgs() );
			$httpCode = wp_remote_retrieve_response_code( $cached );

			if ( $httpCode != '200' ) {
				$response = $httpCode;
				set_site_transient( 'wpt_edd_products_' . $locale, array( 'products' => array() ), DAY_IN_SECONDS );
			} else {
				$response = json_decode( wp_remote_retrieve_body( $cached ) );
				set_site_transient( 'wpt_edd_products_' . $locale, json_decode( wp_remote_retrieve_body( $cached ) ), DAY_IN_SECONDS );
			}

		} else {
				$response = $cached;
		}
		return $response;
	}

	public static function getProducts( $store ) {

		$endpoint = $store['url'] . '/edd-api/products?&number=-1';
		return self::getRemoteData( $endpoint, $store['locale'] );
	}

}
