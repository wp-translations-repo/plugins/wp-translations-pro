<?php

namespace WP_Translations_Pro\APIs;

/**
 * Classe to communicate with the EDD SL API
 *
 * @author     WP-Translations Team
 * @link       https://wp-translations.pro
 * @since      1.0.0
 *
 * @package    WPT_Translations_Pro
 */

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\WordPress\Helpers\ProductHelper;

/**
 * Class from EDD API
 *
 * @since 1.0.0
 */
abstract class EDD_SL_Api {

  protected static function httpArgs( $body ) {

    $args = array(
      'body'        => $body,
    );

    return $args;
  }

  protected static function getRemoteData( $slug, $args, $locale ) {

    $stores = ProductHelper::STORES;
    $storeUrl = $stores[ $locale ]['url'];
    $verify_ssl = (bool) apply_filters( 'edd_sl_api_request_verify_ssl', true );

    if ( strpos( $storeUrl, 'https://' ) !== false && strpos( $storeUrl, 'edd_action=package_download' ) ) {
      $args['sslverify'] = $verify_ssl;
    }

    $cached   = wp_remote_post( $storeUrl, $args );
    $httpCode = wp_remote_retrieve_response_code( $cached );

    if ( $httpCode != '200' ) {
      $response = $httpCode;
    } else {
      $response = json_decode( wp_remote_retrieve_body( $cached ) );
    }

    return $response;
  }

   public static function activateLicense( $slug, $locale ) {

    $product = ProductHelper::getProductInfos( $slug, $locale );

    $body = array(
      'edd_action' => 'activate_license',
      'license'    => $product['license'],
      'item_name'  => urlencode( $product['name'] ),
      'url'        => home_url(),
    );

    return self::getRemoteData( $slug, self::httpArgs( $body ), $locale );
  }

  public static function deactivateLicense( $slug, $locale ) {

    $product = ProductHelper::getProductInfos( $slug, $locale );

    $body = array(
      'edd_action' => 'deactivate_license',
      'license'    => $product['license'],
      'item_name'  => urlencode( $product['name'] ),
      'url'        => home_url(),
    );

    return self::getRemoteData( $slug, self::httpArgs( $body ), $locale );
  }

  public static function checkLicense( $slug, $locale ) {

    $cached = get_site_transient( 'wpt_license_' . $locale .'_' . $slug );
    if ( false === $cached ) {

      $product = ProductHelper::getProductInfos( $slug, $locale );

      $body = array(
        'edd_action' => 'check_license',
        'license'    => $product['license'],
        'item_name'  => urlencode( $product['name'] ),
        'url'        => home_url(),
      );

      $data = self::getRemoteData( $slug, self::httpArgs( $body ), $locale );
      set_site_transient( 'wpt_license_' . $locale .'_' . $slug, true, DAY_IN_SECONDS );

    } else {
      return;
    }

    return $data;
  }

  public static function getVersion( $slug, $locale ) {

    $cached = get_site_transient( 'wpt_version_' . $locale .'_' . $slug );
    if ( false === $cached ) {

      $product = ProductHelper::getProductInfos( $slug, $locale );

      $body = array(
        'edd_action' => 'get_version',
        'item_name'  => urlencode( $product['name'] ),
        'license'    => $product['license'],
        'url'        => home_url(),
      );

      $data = self::getRemoteData( $slug, self::httpArgs( $body ), $locale );
      set_site_transient( 'wpt_version_' . $locale .'_' . $slug, $data, DAY_IN_SECONDS );

    } else {
      $data = $cached;
    }

    return $data;
  }

   public static function getReadme( $slug, $locale ) {

    $products = ProductHelper::localProducts();
    $product  = $products[ $locale ][ $slug ];

    $body = array(
     'edd_action' => 'get_version',
     'item_name'  => urlencode( $product['title'] ),
    );

    $cached = get_site_transient( 'wpt_readme_'. $slug . '_' . $locale );
    if( false === $cached ) {
      $cached = self::getRemoteData( $slug, self::httpArgs( $body ), $locale );
      set_site_transient( 'wpt_readme_'. $slug . '_' . $locale, $cached, DAY_IN_SECONDS );
      $response = $cached;
    } else {
      $response = $cached;
    }
    return $response;
  }

}
