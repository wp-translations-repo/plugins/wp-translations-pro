<?php

namespace WP_Translations_Pro\Models;

/**
 *
 * @author Jerome Sadler
 *
 */

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

interface HooksInterface {

		/**
		 * @return void
		 */
		public function hooks();
}
