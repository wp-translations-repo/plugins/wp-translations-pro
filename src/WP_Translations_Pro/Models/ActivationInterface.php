<?php

namespace WP_Translations_Pro\Models;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

interface ActivationInterface {
    public function activation();
}
