<?php

namespace WP_Translations_Pro\ThirdParty;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\Models\HooksInterface;

/**
 * Specfic methods for Elegant Themes plugins and themes
 *
 * @since 1.0.0
 */

class ElegantThemes implements HooksInterface {

  /**
   * @see WP_Translations_Pro\Models\HooksInterface
   */
  public function hooks() {

    $theme = wp_get_theme();
    if ( 'Divi' === $theme->get( 'Name' ) ) {
      add_action( 'upgrader_process_complete', 'et_pb_force_regenerate_templates' );
      add_action( 'wp_enqueue_scripts', array( $this, 'addDiviFrontStyles') );
      add_action( 'admin_enqueue_scripts', array( $this, 'addDiviAdminStyles') );
    }

  }

  public function addDiviFrontStyles() {

    $css_ext = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '.css' : '.min.css';

    wp_register_style(
      'wptpro-divi-front-styles',
      WPTPRO_PLUGIN_URL . 'assets/css/wpt-divi-front-styles' . $css_ext,
      array(),
      WPTPRO_VERSION
    );
    wp_enqueue_style( 'wptpro-divi-front-styles' );

  }

  public function addDiviAdminStyles() {

    $css_ext = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '.css' : '.min.css';

    wp_register_style(
      'wptpro-divi-admin-styles',
      WPTPRO_PLUGIN_URL . 'assets/css/wpt-divi-admin-styles' . $css_ext,
      array(),
      WPTPRO_VERSION
    );
    wp_enqueue_style( 'wptpro-divi-admin-styles' );

  }

}
