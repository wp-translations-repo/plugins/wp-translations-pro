<?php
namespace WP_Translations_Pro\WordPress\Async;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\WordPress\Async\WPAsyncRequest;
use WP_Translations_Pro\WordPress\Helpers\Helper;
use WP_Translations_Pro\APIs\EDD_SL_Api;

class LicenseAsyncRequest extends WPAsyncRequest {

  protected $action = 'wpt_licenses_async';

  /**
   * Task
   *
   * Override this method to perform any actions required on each
   * queue item. Return the modified item for further processing
   * in the next pass through. Or, return false to remove the
   * item from the queue.
   *
   * @param mixed $item Queue item to iterate over
   *
   * @return mixed
   */
  protected function handle() {
    $options  = Helper::getOptions();
    $api = EDD_SL_Api::checkLicense( $_POST['slug'], $_POST['locale'] );
    if ( is_object( $api ) ) {
      $options['licenses'][ $_POST['locale'] ][ $_POST['slug'] ]['data'] = $api;
    }
    Helper::updateOptions( $options );

    return false;
  }

}
