<?php

namespace WP_Translations_Pro\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

abstract class PageHelper {

  const PRIMARY_PAGE = 'licenses';

  public static function getPrimaryPage() {
    return apply_filters( WPTPRO_SLUG . '_primary_page', self::PRIMARY_PAGE );
  }

  public static function getPages() {

    $pages = array(

      'logs' => array(
        'label' => __( 'Logs', 'wp-translations-pro' ),
        'icon'  => 'dashicons-backup',
        'order' => '90',
      ),
      'settings' => array(
        'label' => __( 'Settings', 'wp-translations-pro' ),
        'icon'  => 'dashicons-admin-settings',
        'order' => '99',
      )
    );

    $pages = apply_filters( WPTPRO_SLUG . '_pages', $pages );

    uasort( $pages,  function( $a, $b ) {
      return $a['order'] - $b['order'];
    });

    return $pages;

  }

  public static function getPage( $slug ) {
    $pages = self::getPages();
    return $pages[ $slug ];
  }

}
