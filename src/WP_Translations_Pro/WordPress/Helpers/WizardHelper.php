<?php

namespace WP_Translations_Pro\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

/**
 *
 * @author Jerome SADLER
 * @since 1.1.6
 */
abstract class WizardHelper {

	public static function detectOlderProducts() {

		$errors           = array();
		$plugins          = get_plugins();
		$pluginsToDestroy = ProductHelper::oldProducts();
		$products         = ProductHelper::localProducts();
		$locale           = get_user_locale();

		foreach( $plugins as $key => $plugin ) {
			$productSlug = str_replace( '-french', '', $plugin['TextDomain'] );
			if ( in_array( $key, $pluginsToDestroy ) ) {
				$errors[ $productSlug ]              = $plugin;
				$errors[ $productSlug ]['license']   =  str_replace( '-', '_', $plugin['TextDomain'] ) . '_license_key';
				$errors[ $productSlug ]['product']   = $productSlug;
				$errors[ $productSlug ]['name']      = $plugin['Name'];
				$errors[ $productSlug ]['type']      = 'plugins';
				$errors[ $productSlug ]['lang']      = $locale;
				$errors[ $productSlug ]['path']      = $key;
			}

		}

		return $errors;
	}

	public static function getSteps() {

		$steps = array(
			'0' => 'Start',
			'1' => 'Migrate',
			'2' => 'License',
			'3' => 'Configure',
			'4' => 'finished'
		);

		return $steps;
	}

}
