<?php

namespace WP_Translations_Pro\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

/**
 *
 * @author Jerome SADLER
 * @since 1.0.0
 */
abstract class TranslationHelper {

  public static function getLocalPoRevisionDate( $translations, $slug, $locale ) {

    if ( isset( $translations[ $slug ][ $locale ] ) || 'divi-builder' == $slug || 'facetwp' == $slug || 'edd-software-licensing' == $slug ) {

      switch ( $slug ) {
        case 'Divi':
          $textdomains = array(
            'Divi'       => ( isset( $translations['Divi'] ) ) ? strtotime( $translations['Divi'][ $locale ]['PO-Revision-Date'] ) : 0,
          );
          $poRevision = max( $textdomains );
          break;

        case 'bloom':
          $textdomains = array(
            'bloom'        =>  ( isset( $translations['bloom'] ) ) ? strtotime( $translations['bloom'][ $locale ]['PO-Revision-Date'] ) : 0,
          );
          $poRevision = max( $textdomains );
          break;

        case 'extra':
          $textdomains = array(
            'extra'       => ( isset( $translations['extra'] ) ) ? strtotime( $translations['extra'][ $locale ]['PO-Revision-Date'] ) : 0,
            'Extra'       => ( isset( $translations['Extra'] ) ) ? strtotime( $translations['Extra'][ $locale ]['PO-Revision-Date'] ) : 0,

          );
          $poRevision = max( $textdomains );
          break;

        case 'divi-builder':
          $textdomains = array(
            'et_builder_plugin' => ( isset( $translations['et_builder_plugin'] ) ) ? strtotime( $translations['et_builder_plugin'][ $locale ]['PO-Revision-Date'] ) : 0,
          );
          $poRevision = max( $textdomains );
          break;

        case 'facetwp':
          $textdomains = array(
            'fwp'       => ( isset( $translations['fwp'] ) ) ? strtotime( $translations['fwp'][ $locale ]['PO-Revision-Date'] ) : 0,
            'fwp-front' => ( isset( $translations['fwp-front'] ) ) ? strtotime( $translations['fwp-front'][ $locale ]['PO-Revision-Date'] ) : 0,
          );
          $poRevision = max( $textdomains );
          break;

          case 'edd-software-licensing':
            $textdomains = array(
              'edd_sl' => ( isset( $translations['edd_sl'] ) ) ? strtotime( $translations['edd_sl'][ $locale ]['PO-Revision-Date'] ) : 0,
            );
            $poRevision = max( $textdomains );
            break;

        default:
          $poRevision = strtotime( $translations[ $slug ][ $locale ]['PO-Revision-Date'] );
          break;
      }

    } else {
     $poRevision = 0;
    }

    return $poRevision;
  }

  public static function rewriteTextdomain( $slug ) {

    $domainsToRewrite = array(
      'divi'    => 'Divi',
      'monarch' => 'Monarch'
    );

    $rewrite = ( in_array( $slug, array_keys( $domainsToRewrite ) ) ) ? $domainsToRewrite[ $slug ] : $slug;

    return $rewrite;
  }

  public static function sanitizeTextdomain( $slug ) {
    return sanitize_title( $slug );
  }

}
