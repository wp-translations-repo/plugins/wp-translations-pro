<?php

namespace WP_Translations_Pro\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

/**
 *
 * @author Jerome SADLER
 * @version 1.0.0
 * @since 1.0.0
 */
abstract class FileHelper {

  /**
   * Directory creation based on WordPress Filesystem
   *
   * @since 1.0.0
   *
   * @param string $dir The path of directory will be created.
   * @return bool
   */
  public static function makeDir( $dir ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-filesystem-base.php' );
    require_once( ABSPATH . 'wp-admin/includes/class-wp-filesystem-direct.php' );
    $direct_filesystem = new \WP_Filesystem_Direct( new \StdClass() );

    $chmod = defined( 'FS_CHMOD_DIR' ) ? FS_CHMOD_DIR : ( fileperms( WP_CONTENT_DIR ) & 0777 | 0755 );
    return $direct_filesystem->mkdir( $dir, $chmod );
  }

  /**
   * Recursive directory creation based on full path.
   *
   * @since 1.0.0
   *
   * @source wp_mkdir_p() in /wp-includes/functions.php
   */
   public static function makeRecursiveDir( $target ) {
      // From php.net/mkdir user contributed notes.
      $target = str_replace( '//', '/', $target );

      // safe mode fails with a trailing slash under certain PHP versions.
      $target = rtrim( $target, '/' ); // Use rtrim() instead of untrailingslashit to avoid formatting.php dependency.
      if ( empty( $target ) ) {
        $target = '/';
      }

      if ( file_exists( $target ) ) {
        return @is_dir( $target );
      }

      // Attempting to create the directory may clutter up our display.
      if ( self::makeDir( $target ) ) {
        return true;
      } elseif ( is_dir( dirname( $target ) ) ) {
        return false;
      }

      // If the above failed, attempt to create the parent node, then try again.
      if ( ( $target != '/' ) && ( self::makeRecursiveDir( dirname( $target ) ) ) ) {
        return self::makeRecursiveDir( $target );
      }

      return false;
  }

  /**
   * File creation based on WordPress Filesystem
   *
   * @since 1.0.0
   *
   * @param string $file 	  The path of file will be created.
   * @param string $content The content that will be printed.
   * @return bool
   */
  public static function putContent( $file, $content ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-filesystem-base.php' );
    require_once( ABSPATH . 'wp-admin/includes/class-wp-filesystem-direct.php' );
    $direct_filesystem = new \WP_Filesystem_Direct( new \StdClass() );

    $chmod = defined( 'FS_CHMOD_FILE' ) ? FS_CHMOD_FILE : 0644;
    return $direct_filesystem->put_contents( $file, (string) $content, $chmod );
  }

  /**
   * Scan content dir
   *
   * @since 1.0.0
   *
   * @return array
   */
  public static function scandir( $dir ) {
    $target = WPTU_CONTENT_PATH. '/' . $dir;
    if ( is_dir( $target ) ) {
      $resources = scandir( $target );
      return $resources;
    }
  }

  /**
   * Remove all mo files
   *
   * @param string $dir filename.
   * @since 1.0.0
   */
  public static function removeRecursiveDir( $dir ) {
    if ( ! is_dir( $dir ) ) {
      // @codingStandardsIgnoreStart
      unlink( $dir );
      // @codingStandardsIgnoreEnd
      return;
    }
    if ( $globs = glob( $dir . '/*', GLOB_NOSORT ) ) {
      foreach ( $globs as $file ) {
        // @codingStandardsIgnoreStart
        is_dir( $file ) ? self::removeRecursiveDir( $file ) : unlink( $file );
        // @codingStandardsIgnoreEnd
      }
    }
    // @codingStandardsIgnoreStart
    rmdir( $dir );
    // @codingStandardsIgnoreStart
  }

  /**
   * Create zip file
   *
   * @since 1.1.5
   *
   * @return array
   */
  public static function createZip( $files = array(), $destination = '', $overwrite = false ) {
    //if the zip file already exists and overwrite is false, return false
    if( file_exists( $destination ) && !$overwrite ) { return false; }
    //vars
    $valid_files = array();
    //if files were passed in...
    if ( is_array( $files ) ) {
      //cycle through each file
      foreach( $files as $file ) {
        //make sure the file exists
        if ( file_exists( $file ) ) {
          $valid_files[] = $file;
        }
      }
    }
    //if we have good files...
    if ( count( $valid_files ) ) {
      //create the archive
      $zip = new ZipArchive();
      if ( $zip->open( $destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE ) !== true ) {
        return 'false';
      }
      //add the files
      foreach( $valid_files as $file ) {
        $zip->addFile( $file, basename( $file ) );
      }

      //close the zip -- done!
      $zip->close();

      //check to make sure the file exists
      return file_exists( $destination );
    }	else {
      return false;
    }
  }

}
