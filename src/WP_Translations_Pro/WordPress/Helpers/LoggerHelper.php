<?php

namespace WP_Translations_Pro\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\WordPress\Helpers\LicenseHelper;

/**
 *
 * @author Jerome SADLER
 * @since 1.0.0
 */
abstract class LoggerHelper {

  public static function log( $license, $action, $serverData, $response = false, $version = false ) {

    $logs = get_site_option( 'wpt_pro_logs' );
    $time = current_time( 'timestamp' );

    switch ( $action ) {
      case 'activate':
        $message = esc_html__( 'Activation', 'wp-translations-pro' );
        $type = 'license';
        $status['class']   = 'success';
        break;
      case 'deactivate':
        $message = esc_html__( 'Deactivation', 'wp-translations-pro' );
        $type = 'license';
        $status['class']   = 'success';
            break;
      case 'save':
        $message = esc_html__( 'Saving', 'wp-translations-pro' );
        $type = 'license';
        $status['class']   = 'success';
        break;
      case 'delete':
        $message = esc_html__( 'Deletion', 'wp-translations-pro' );
        $type = 'license';
        $status['message'] = esc_html__( 'Deleted', 'wp-translations-pro' );
        $status['class']   = 'success';
        break;
      case 'update':
        $message = esc_html__( 'Updated', 'wp-translations-pro' );
        $type = 'update';
        $status['message'] = esc_html__( 'Success', 'wp-translations-pro' );
        $status['class']   = 'success';
        break;
      case 'update-available':
        $message = esc_html__( 'Update available', 'wp-translations-pro' );
        $type = 'update';
        $status['message'] = esc_html__( 'Waiting', 'wp-translations-pro' );
        $status['class']   = 'waiting';
        break;
    }

    if ( is_object( $response ) ) {
      $status            = LicenseHelper::humanStatus( $response );
    } else {
      $status['message'] = esc_html__( 'Success', 'wp-translations-pro' );
      $status['class']   = 'success';
    }

    if ( 'update' == $action || 'update-available' == $action ) {
      $logs[ $type ][ $action . '-' . $version['po_revision'] ] = array(
        'action'      => $action,
        'textdomain'  => $license,
        'title'       => $message,
        'date'        => $time,
        'server'      => $serverData,
        'response'    => $response,
        'status'      => $status,
        'version'     => $version
      );
    } else {
      $logs[ $type ][ $time ] = array(
        'action'   => $action,
        'license'  => self::maskLicense( $license, null, strlen( $license ) -4 ),
        'title'    => $message,
        'date'     => $time,
        'server'   => $serverData,
        'response' => $response,
        'status'   => $status
      );
    }
    if ( ! empty ( $logs['license'] ) ) {
      krsort( $logs['license'], SORT_NUMERIC );
    }

    update_site_option( 'wpt_pro_logs', $logs );
  }

  public static function maskLicense( $str, $start = 0, $length = null ) {

    $mask = preg_replace ( "/\S/", "*", $str );
    if ( is_null ( $length ) ) {
        $mask = substr ( $mask, $start );
        $str  = substr_replace ( $str, $mask, $start );
    } else {
        $mask = substr ( $mask, $start, $length );
        $str  = substr_replace ( $str, $mask, $start, $length );
    }
    return $str;
  }

}
