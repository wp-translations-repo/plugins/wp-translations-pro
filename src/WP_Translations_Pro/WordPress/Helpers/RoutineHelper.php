<?php

namespace WP_Translations_Pro\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

/**
 *
 * @author Jerome SADLER
 * @since 1.2.5
 */
abstract class RoutineHelper {

  public static function triggerUpgrades() {

    $pluginVersion = get_site_option( 'wpt_pro_version' );

    if ( $pluginVersion && true === version_compare( $pluginVersion, '1.2.6', '<'  ) ) {

      $routine = self::upgrade_125();

    }

    if ( ! $pluginVersion ) {
      
      delete_site_transient( 'wpt_edd_products_fr_FR' );
      $pluginVersion = WPTPRO_VERSION;
      add_site_option( 'wpt_pro_version', $pluginVersion );

    }

  }

  public static function upgrade_125() {

    delete_site_transient( 'wpt_edd_products_fr_FR' );
    $pluginVersion = WPTPRO_VERSION;
    update_site_option( 'wpt_pro_version', $pluginVersion );

  }

}
