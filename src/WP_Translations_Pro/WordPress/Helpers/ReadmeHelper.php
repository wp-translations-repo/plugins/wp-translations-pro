<?php

namespace WP_Translations_Pro\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\WordPress\Helpers\ProductHelper;
use WP_Translations_Pro\APIs\EDD_SL_Api;

/**
 *
 * @author Jerome SADLER
 * @since 1.0.4
 */
abstract class ReadmeHelper {

  public static function displayReadmeModal( $slug, $locale, $args = array() ) {

    $defaults = array(
      'show_button'      => true,
      'button_css_class' => 'wpt-button-link',
      'button_text'      => __( 'View details', 'wp-translations-pro' ),
      'button_icon'      => '',
      'active_tab'       => 'description'
    );
    $args = wp_parse_args( $args, $defaults );

    $readme    = EDD_SL_Api::getReadme( $slug, $locale );
    $products  = ProductHelper::localProducts();
    $product   = $products[ $locale ][ $slug ];

    $buttonIcon = ( ! empty( $args['button_icon'] ) ) ? '<span class="dashicons ' . esc_attr( $args['button_icon'] ) . '"></span> ' : '';
    ?>

    <?php if ( false !== $args['show_button'] ) : ?>
    <button class="js-modal <?php echo esc_attr( $args['button_css_class'] ); ?>"
          data-modal-prefix-class="wpt-modal"
          data-modal-content-id="wpt-readme-<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $locale ); ?>"
          data-modal-close-text="<span><?php esc_html_e( 'Close modal', 'wp-translations-pro'); ?></span>"
          data-modal-close-title="<?php esc_html_e( 'Close modal', 'wp-translations-pro'); ?>"
          title="<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $locale ); ?>"><?php echo $buttonIcon; ?> <?php echo esc_html( $args['button_text'] ); ?></button>
    <?php endif; ?>

    <div id="wpt-readme-<?php echo esc_attr( $slug ); ?>-<?php echo esc_attr( $locale ); ?>" class="hidden">

      <?php if ( ! empty ( $readme->banners ) ) :
        $banners = unserialize( $readme->banners );
        if( ! empty( $banners['high'] )) :?>
          <img src="<?php echo esc_url( $banners['high'] ); ?>" style="max-width:100%;" />
        <?php else : ?>
          <img src="<?php echo esc_url( $banners['low'] ); ?>" style="max-width:100%;" />
      <?php endif;
       endif;?>

       <div class="inside">
         <ul>
           <?php if ( ! empty( $readme->stable_version ) ) : ?>
             <li><strong><?php esc_html_e( 'Version:', 'wp-translations-pro' ); ?></strong> <?php echo $readme->stable_version; ?> rev(<?php echo strtotime( $readme->po_revision ); ?>)</li>
           <?php endif; ?>
           <?php if ( ! empty( $readme->po_revision ) ) : ?>
             <li><strong><?php esc_html_e( 'Last Update:', 'wp-translations-pro' ); ?></strong> <?php echo date_i18n( get_option( 'date_format' ),strtotime( $readme->po_revision ) ); ?></li>
           <?php endif; ?>
         </ul>
         <?php if( isset( $readme->contributors ) ) : ?>
           <h3><?php esc_html_e( 'Contributors:', 'wp-translations-pro' ); ?></h3>
           <?php foreach ( $readme->contributors as $contributor ) : ?>
             <a href="https://profiles.wordpress.org/<?php echo $contributor; ?>"><?php echo $contributor; ?></a>
          <?php endforeach;
         endif; ?>
       </div>

      <div class="js-tabs inside" data-tabs-disable-fragment="1">
        <ul class="js-tablist" data-hx="h2" data-tabs-prefix-class="wpt-readme">
        <?php if ( isset( $readme ) && ! empty( $readme ) ) :
          $sections = unserialize( $readme->sections );
          foreach ( $sections as $key => $section ) :
            $activeTab = ( $key == $args['active_tab'] ) ? 'data-selected="1"' : '';
            if ( !empty( $section ) ) : ?>
          <li class="js-tablist__item">
            <a href="#<?php echo $key; ?>_<?php echo esc_attr( $locale ); ?>_<?php echo esc_attr( $slug ); ?>" id="label_<?php echo $key; ?>_<?php echo $locale; ?>_<?php echo esc_attr( $slug ); ?>" class="js-tablist__link" <?php echo $activeTab; ?>><?php echo ProductHelper::getReadmeSections( $key ); ?></a>
          </li>
            <?php endif;
          endforeach; ?>
        </ul>

        <?php foreach ( $sections as $key => $section ) :
          if ( !empty( $section ) ) : ?>
            <div id="<?php echo $key; ?>_<?php echo esc_attr( $locale ); ?>_<?php echo esc_attr( $slug ); ?>" class="js-tabcontent">
              <?php print_r( $section ); ?>
              <?php if( 'description' == $key ) : ?>
                <p class="wpt-product-page-link"><a href="<?php echo esc_url( $product['link'] ) ?>" class="wpt-button" target="_blank"><span class="dashicons dashicons-store"></span> <?php esc_html_e( 'Go to Store', 'wp-translations-pro' ); ?></a></p>
              <?php endif; ?>
            </div>
          <?php endif;
        endforeach; ?>
        <?php endif; ?>
      </div>
    </div>
    <?php
  }

}
