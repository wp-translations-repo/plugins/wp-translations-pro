<?php

namespace WP_Translations_Pro\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

/**
 *
 * @author Jerome SADLER
 * @since 1.0.0
 */
abstract class Helper {

    public static function adminScreen() {

      $screens = array(
        'toplevel_page_' . WPTPRO_SLUG,
        'toplevel_page_' . WPTPRO_SLUG . '-network',
        'settings_page_' . WPTPRO_SLUG
      );

      return $screens;
    }

		public static function assetsScreen() {

      $admin  = self::adminScreen();
      $assets = array(
        'plugins',
        'themes',
      );

      $screens = array_merge( $admin, $assets );

      return $screens;
    }


    public static function adminUrl( $args = false ) {
      $url = is_multisite() ? network_admin_url( $args ) : admin_url( $args );

      return esc_url( $url );
    }

    public static function getOptions() {

      $defaults = array(
        'settings' => array(
          'core_updates'    => 1,
          'plugins_updates' => 1,
          'themes_updates'  => 1,
          'bubble_count'    => 1,
          'page_hook'       => 'menu',
          'debug'           => '',
        ),
        'licenses'        => array(),
      );

      $options = ( false === get_site_option( 'wpt_pro_settings' ) ) ? $defaults : get_site_option( 'wpt_pro_settings' );
      return $options;
    }

    public static function updateOptions( $options ) {
      update_site_option( 'wpt_pro_settings', $options );
    }

		public static function displayArrayRecursively( $arr ) {
      if ( $arr ) {
          foreach ( $arr as $key => $value ) {

              if ( is_array( $value ) || is_object( $value ) ) {
                $type = ( is_object( $value ) ) ? 'object' : 'array';
                echo '<h4 class="js-expandmore">' . $key . ' (' . $type . ')</h4>
                <div class="js-to_expand">
                <ul>';
                  self::displayArrayRecursively( $value );
                echo '</ul></div>';
              } else {
                  //  Output
                  echo '<li class="'. $key .'"><div><span class="wpt-debug-key">' . $key . '</span>: <span class="wpt-debug-value">' . $value . '</span></div></li>';
              }

          }
      }
  }


  public static function clearUpdateCache( $type = 'all' ) {

    switch ( $type ) {
      case 'plugins':
        wp_clean_plugins_cache();
        wp_update_plugins();
        break;
      case 'themes':
        wp_clean_themes_cache();
        wp_update_themes();
        break;
      case 'all':
        wp_clean_update_cache();
        wp_update_plugins();
        wp_update_themes();
        break;
    }

  }

	public static function generateUpdateCache( $type = 'all' ) {

    self::clearUpdateCache( $type );

    switch ( $type ) {
      case 'plugins':
        wp_update_plugins();
        break;
      case 'themes':
        wp_update_themes();
        break;
      case 'all':
        wp_update_plugins();
        wp_update_themes();
        break;
    }

  }


}
