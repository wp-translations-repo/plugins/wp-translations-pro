<?php

namespace WP_Translations_Pro\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\APIs\EDD_Api;
use WP_Translations_Pro\WordPress\Helpers\LicenseHelper;

/**
 *
 * @author Jerome SADLER
 * @since 1.0.0
 */
abstract class ProductHelper {

  const STORES = array (
    'fr_FR' => array(
      'url'      => 'https://fxbenard.com',
      'checkout' => '/commander/',
      'prefix'   => '-french',
      'locale'   => 'fr_FR',
    )
  );

  public static function localProducts() {

    $products = get_site_transient( 'wpt_pro_localproducts' );

    if ( false === $products ) {

      if ( ! function_exists( 'get_plugins' ) ) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
      }

      foreach ( self::STORES as $store ) {

        $remoteProducts = (array) EDD_Api::getProducts( $store );

  			if ( isset( $remoteProducts['products'] ) && ! empty( $remoteProducts['products'] ) ) {
  	      foreach( $remoteProducts['products'] as $remoteProduct ) {
  	        $slug = str_replace( $store['prefix'], '', $remoteProduct->info->slug );
  	        $products[ $store['locale'] ][ $slug ] = array(
  	          'slug'    => $slug,
  	          'id'			=> $remoteProduct->info->id,
  	          'title'   => $remoteProduct->info->title,
  	          'link'    => $remoteProduct->info->link,
  	          'updated' => $remoteProduct->info->modified_date,
  	          'version' => $remoteProduct->licensing->version,
  	          'pricing' => count( (array) $remoteProduct->pricing ),
  	        );
  	      }
  			}

        //local
        $plugins_active = get_plugins();
        $plugins = array();
        foreach ( $plugins_active as $file => $data ) {
          $domains[ $data['TextDomain'] ] = 'plugin';
        }

        $themes = wp_get_themes();
        foreach ( $themes as $key => $theme ) {
          if( ! empty( $theme->get( 'TextDomain' ) ) ) {
            $domains[ $theme->get( 'TextDomain' ) ] = 'theme' ;
          } else {
            $domains[ sanitize_title( $theme->get( 'Name' ) ) ] = 'theme' ;
          }
        }

        foreach ( $products as $locale => $store ) {
          foreach ( $store as $key => $product ) {
            if ( ! in_array( $product['slug'], array_keys( $domains ) ) )  {
              unset( $products[ $locale ][ $key ] );
            } else {
              $products[ $locale ][ $key ]['type'] = $domains[ $key ];
            }
          }
        }

      }
      set_site_transient( 'wpt_pro_localproducts', $products, DAY_IN_SECONDS );
    }

    return $products;
  }

  public static function isProduct( $slug, $locale ) {

    $products = self::localProducts();

    if ( isset( $products[ $locale ][ $slug ] ) ) {
      return true;
    } else {
      return false;
    }

  }

  public static function getProductInfos( $slug, $locale ) {

    $infos = get_site_option( 'wpt_pro_settings' );

    return $infos['licenses'][ $locale ][ $slug ];
  }

   public static function getReadmeSections( $key ) {

     switch ( $key ) {
       case 'changelog':
         $label = esc_html__( 'Changelog', 'wp-translations-pro' );
         break;

       case 'installation':
         $label = esc_html__( 'Installation', 'wp-translations-pro' );
         break;

       case 'description':
         $label = esc_html__( 'Description', 'wp-translations-pro' );
         break;

       default:
         $label = $key;
         break;
     }

     return $label;

   }

  public static function isToPromote( $slug, $locale ) {

    if ( false !== self::isProduct( $slug, $locale ) && false === LicenseHelper::isValid( $slug, $locale ) ) {
      return true;
    } else {
      return false;
    }

  }

	public static function oldProducts() {

		$pluginsToDestroy = array(
			'divi-french/divi-french.php',
			'extra-french/extra-french.php',
			'bloom-french/bloom-french.php',
			'divi-builder-french/divi-builder-french.php'
		);

		return $pluginsToDestroy;
	}

}
