<?php

namespace WP_Translations_Pro\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\APIs\EDD_Api;
use WP_Translations_Pro\APIs\EDD_SL_Api;

/**
 *
 * @author Jerome SADLER
 * @since 1.0.0
 */
abstract class LicenseHelper {

  public static function isSave( $slug, $locale ) {
    $options = get_site_option( 'wpt_pro_settings' );
    if ( isset( $options['licenses'][ $locale ][ $slug ] ) ) {
      return true;
    } else {
			return false;
		}
  }

  public static function isValid( $slug, $locale ) {
    $options = get_site_option( 'wpt_pro_settings' );
    if ( isset( $options['licenses'][ $locale ][ $slug ]['data'] ) ) {
      $status = ( 'valid' == $options['licenses'][ $locale ][ $slug ]['data']->license ) ? true : false;
    } else {
      $status = false;
    }
    return $status;
  }

  public static function humanStatus( $data ) {
    if ( isset( $data->license ) && ! empty( $data->license ) ) {
      switch ( $data->license ) {

        case 'valid' :
          $status['message'] = esc_html__( 'Activated', 'wp-translations-pro' );
          $status['class']   = 'success';
          break;

        case 'deactivated' :
          $status['message'] = esc_html__( 'Deactivated', 'wp-translations-pro' );
          $status['class']   = 'success';
          break;

        case 'expired':
          $status['message'] = esc_html__( 'Expired', 'wp-translations-pro' );
          $status['class']   = 'error';
          break;

        case 'site_inactive' :
          $status['message'] = esc_html__( 'Inactive Site', 'wp-translations-pro' );
          $status['class']   = 'error';
          break;

        case 'inactive' :
          $status['message'] = esc_html__( 'Inactive Key', 'wp-translations-pro' );
          $status['class']   = 'error';
          break;

        case 'invalid' :

          switch ( $data->error ) {

            case 'revoked' :
              $status['message'] = esc_html__( 'Disabled', 'wp-translations-pro' );
              $status['class']   = 'error';
              break;

            case 'missing' :
            case 'invalid':
              $status['message'] = esc_html__( 'Invalid', 'wp-translations-pro' );
              $status['class']   = 'error';
              break;

            case 'no_activations_left':
              $status['message'] = esc_html__( 'Activation Limit', 'wp-translations-pro' );
              $status['class']   = 'error';
              break;

            case 'item_name_mismatch' :
              $status['message'] = esc_html__( 'Invalid key for this product', 'wp-translations-pro' );
              $status['class']   = 'error';
              break;

            case 'expired':
              $status['message'] = esc_html__( 'Expired', 'wp-translations-pro' );
              $status['class']   = 'error';
              break;
          }

        break;
      }
      return $status;
    }
  }

  public static function messageStatus( $data ) {

    if ( isset( $data->license ) && ! empty( $data->license ) ) {
      switch ( $data->license ) {

        case 'valid' :
          $message = esc_html__( 'License Activated', 'wp-translations-pro' );
          break;

        case 'expired':
          $message = sprintf(
            esc_html__( 'Your license key expired on %s', 'wp-translations-pro' ),
            date_i18n( get_option( 'date_format' ), strtotime( $data->expires, current_time( 'timestamp' ) ) )
          );
          break;

        case 'site_inactive' :
          $message = esc_html__( 'Your license is not active for this site', 'wp-translations-pro' );
          break;

        case 'inactive' :
          $message = esc_html__( 'Inactive License Key', 'wp-translations-pro' );
          break;

        case 'invalid' :


            switch ( $data->error ) {

              case 'revoked' :
                $message = esc_html__( 'Your license key has been disabled', 'wp-translations-pro' );
                break;

              case 'missing' :
              case 'invalid':
                $message = esc_html__( 'Invalid license', 'wp-translations-pro' );
                break;

              case 'no_activations_left':
                $message = esc_html__( 'Your license key has reached its activation limit', 'wp-translations-pro' );
                break;

              case 'item_name_mismatch' :
                $message = esc_html__( 'This appears to be an invalid license key for this product', 'wp-translations-pro' );
                break;

              case 'expired':
                $message = sprintf(
                  esc_html__( 'Your license key expired on %s', 'wp-translations-pro' ),
                  date_i18n( get_option( 'date_format' ), strtotime( $data->expires, current_time( 'timestamp' ) ) )
                );
                break;
            }


        break;
      }
      return $message;
    }


  }

}
