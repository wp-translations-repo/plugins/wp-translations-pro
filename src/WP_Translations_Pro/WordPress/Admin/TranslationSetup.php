<?php

namespace WP_Translations_Pro\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\Models\HooksAdminInterface;
use WP_Translations_Pro\WordPress\Helpers\Helper;
use WP_Translations_Pro\WordPress\Helpers\ProductHelper;
use WP_Translations_Pro\WordPress\Helpers\TranslationHelper;
use WP_Translations_Pro\WordPress\Helpers\ReadmeHelper;
use WP_Translations_Pro\WordPress\Admin\TranslationNotification;

/**
 * Translations Setup
 *
 * @since 1.0.0
 */

class TranslationSetup implements HooksAdminInterface {

  public function __construct() {
    $this->options = Helper::getOptions();
    $this->locale  = get_user_locale();
  }

  public function hooks() {

    add_action( 'init', array( $this, 'translationsNotifications' ) );

    if ( false !== (bool) $this->options['settings']['bubble_count'] ) {
      add_filter( 'wp_get_update_data', array( $this, 'translationsUpdateCount' ) );
    }
    if ( ! is_multisite() && current_user_can( 'update_languages' ) ) {
      add_action('admin_footer-themes.php', array( $this, 'themesReadmeModal' ) );
    }
  }

  public function translationsNotifications() {

    if ( ! function_exists( 'get_plugins' ) ) {
      require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }

    $translations = wp_list_pluck( wp_get_translation_updates(), 'type', 'slug' );

    $plugins_active = get_plugins();
    $plugins = array();
    foreach ( $plugins_active as $file => $data ) {
      $plugins[ $data['TextDomain'] ] = array(
        'name'       => $data['Name'],
        'textdomain' => $data['TextDomain'],
        'file'       => $file,
      );
      $domains[ $data['TextDomain'] ] = 'plugin';

      if ( false !== ProductHelper::isToPromote( $data['TextDomain'], $this->locale ) ) {
        $status = 'notice-info';
        $notification = new TranslationNotification( $data['TextDomain'], 'plugin', $file, $status );
      }
    }

    $themes = wp_get_themes();
    foreach ( $themes as $key => $theme ) {
      $themes[ $key ] = $theme->get( 'TextDomain' );
      $domains[ $theme->get( 'TextDomain' ) ] = 'theme' ;
    }

    foreach ( $translations as $slug => $type ) {
      $slug = TranslationHelper::sanitizeTextdomain( $slug );
      if ( array_key_exists( $slug, $plugins ) ) {
        $data = ( 'plugin' == $type ) ? $plugins[ $slug ]['file'] : wp_get_theme( $slug );
        if ( false !== (bool) $this->options['settings'][ 'plugins_updates'] || false !== (bool) $this->options['settings'][ 'themes_updates'] )  {
          $notification = new TranslationNotification( $slug, $type, $data, 'update' );
        }
      }
    }

  }

  public function translationsUpdateCount( $update_data ) {

    $update_data['counts']['translations'] = ( 0 < count( wp_get_translation_updates() ) ) ? count( wp_get_translation_updates() ) : '';
    if ( 1 < count( wp_get_translation_updates() ) ) {
      $update_data['counts']['total'] = ( count( wp_get_translation_updates() ) + $update_data['counts']['total'] ) - 1;
    }

    $titles = array();
    if ( $update_data['counts']['wordpress'] ) {
      /* translators: 1: Number of updates available to WordPress */
      $titles['wordpress'] = sprintf( __( '%d WordPress Update', 'wp-translations-pro' ), $update_data['counts']['wordpress'] );
    }
    if ( $update_data['counts']['plugins'] ) {
      /* translators: 1: Number of updates available to plugins */
      $titles['plugins'] = sprintf( _n( '%d Plugin Update', '%d Plugin Updates', $update_data['counts']['plugins'], 'wp-translations-pro' ), $update_data['counts']['plugins'] );
    }
    if ( $update_data['counts']['themes'] ) {
      /* translators: 1: Number of updates available to themes */
      $titles['themes'] = sprintf( _n( '%d Theme Update', '%d Theme Updates', $update_data['counts']['themes'], 'wp-translations-pro' ), $update_data['counts']['themes'] );
    }
    if ( $update_data['counts']['translations'] ) {
      /* translators: 1: Number of updates available to translations */
      $titles['translations'] = sprintf( _n( '%d Translation Update', '%d Translation Updates', $update_data['counts']['translations'], 'wp-translations-pro' ), $update_data['counts']['translations'] );
    }
    $update_data['title'] = $titles ? esc_attr( implode( ', ', $titles ) ) : '';

    return $update_data;
  }

  public function themesReadmeModal() {

    $products  = ProductHelper::localProducts();

		if( array_key_exists( 'fr_FR', $products ) ) {
	    foreach ( $products['fr_FR'] as $product ) {
	      if ( 'theme' == $product['type'] && false !== ProductHelper::isToPromote( $product['slug'], 'fr_FR' ) ) {
	        $modalArgs = array(
	          'show_button'      => false,
	          'active_tab'       => 'description'
	        );
	        ReadmeHelper::displayReadmeModal( $product['slug'], 'fr_FR', $modalArgs );
	      }
	    }
		}

  }

}
