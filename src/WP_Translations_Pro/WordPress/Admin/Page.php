<?php

namespace WP_Translations_Pro\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\Models\HooksAdminInterface;
use WP_Translations_Pro\WordPress\Helpers\Helper;
use WP_Translations_Pro\WordPress\Helpers\PageHelper;

/**
 * Admin Page
 *
 * @since 1.0.0
 */

class Page implements HooksAdminInterface {

  /**
   * @see WP_Translations_Pro\Models\HooksInterface
   */
  public function hooks() {
    add_action( is_multisite() ? 'network_admin_menu' : 'admin_menu', array( $this, 'addMenuPage' ) );
    add_filter( 'update_footer',                                      array( $this, 'updateFooter'), 15 );
    add_filter( 'admin_footer_text',                                  array( $this, 'adminFooterText' ) );
  }

  /**
   * Add options page.
   */
  public function addMenuPage() {

    $options = Helper::getOptions();
    $capability = is_multisite() ? 'manage_network' : 'manage_options';

    if ( 'menu' == $options['settings']['page_hook'] ) {
      add_menu_page(
          'FXB-Translations',
          'FXB-Translations',
          $capability,
          WPTPRO_SLUG,
          array( $this, 'displayPage' ),
          WPTPRO_PLUGIN_URL . '/assets/img/FxB.svg'
      );

			$pages = PageHelper::getPages();

			foreach( $pages as $slug => $page ) {
				add_submenu_page(
          WPTPRO_SLUG,
          $page['label'],
          $page['label'],
          $capability,
          'admin.php?page=' . WPTPRO_SLUG . '&wpt-page=' . $slug,
          ''
        );
			}

    } else {
      add_options_page(
        'FXB-Translations',
        'FXB-Translations',
        $capability,
        'wp-translations-pro',
        array( $this, 'displayPage' )
      );
    }

  }

  public function displayPage() {

	 if (	get_site_option( 'wpt_pro_pending_migration' ) ) {

			require_once  WPTPRO_PLUGIN_DIR_TEMPLATES_ADMIN . '/wizard.php';

		} else {


			require_once  WPTPRO_PLUGIN_DIR_TEMPLATES_ADMIN . '/_header.php';

	    if ( isset( $_GET['wpt-page'] ) && 'settings' == $_GET['wpt-page'] ) {
	      require_once  WPTPRO_PLUGIN_DIR_TEMPLATES_ADMIN . '/settings.php';
	    } elseif ( isset( $_GET['wpt-page'] ) && 'logs' == $_GET['wpt-page'] ) {
	      require_once  WPTPRO_PLUGIN_DIR_TEMPLATES_ADMIN . '/logs.php';
	    } else {
	      require_once  WPTPRO_PLUGIN_DIR_TEMPLATES_ADMIN . '/list-licenses.php';
	    }

	    require_once  WPTPRO_PLUGIN_DIR_TEMPLATES_ADMIN . '/_footer.php';

		}

  }

  /**
   * Custom footer text left
   *
   * @param  string $text Get default text.
   * @return return filterd text.
   * @since 1.0.0
   */
  function adminFooterText( $text ) {
    $screen = get_current_screen();
    if ( ! in_array( $screen->id, Helper::adminScreen() ) ) {
      return $text;
    } else {
      $link_1 = '<a href="https://fxbenard.com/" target="_blank">FX Benard</a>';
      $link_2 = '<a target="_blank" href="https://fxbenard.com/contactez-moi/">';
      $link_3 = '</a>';
      return sprintf( esc_html__( 'Visit %1$s website | %2$sContact Support%3$s', 'wp-translations-pro' ), $link_1, $link_2, $link_3 );
    }
  }


  /**
   * Custom footer text right
   *
   * @param  string $text Get default text.
   * @return return filterd text.
   * @since 1.0.0
   */
  public function updateFooter( $text ) {
    $screen = get_current_screen();
    if ( ! in_array( $screen->id, Helper::adminScreen() ) ) {
      return $text;
    } else {
      $version = esc_html__( 'Version:&nbsp;', 'wp-translations-pro' ) . WPTPRO_VERSION;
      return $version;
    }
  }


}
