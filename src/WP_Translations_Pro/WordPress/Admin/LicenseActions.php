<?php

namespace WP_Translations_Pro\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\Models\HooksAdminInterface;
use WP_Translations_Pro\WordPress\Helpers\Helper;
use WP_Translations_Pro\WordPress\Helpers\LicenseHelper;
use WP_Translations_Pro\WordPress\Helpers\ProductHelper;
use WP_Translations_Pro\WordPress\Helpers\LoggerHelper;
use WP_Translations_Pro\WordPress\Admin\TranslationUpdater;
use WP_Translations_Pro\APIs\EDD_SL_Api;
use WP_Translations_Pro\WordPress\Async\LicenseAsyncRequest;

/**
 * Licenses Actions
 *
 * @since 1.0.0
 */

class LicenseActions implements HooksAdminInterface {

  protected $options;
  protected $products;

  public function __construct() {
    $this->options  = Helper::getOptions();
    $this->products = ProductHelper::localProducts();
		$this->licenseBackground = new LicenseAsyncRequest();
  }

  /**
   * @see WP_Translations\Models\HooksInterface
   */
  public function hooks() {
    add_action( 'wp_ajax_saveLicense',       array( $this, 'saveLicense' ) );
    add_action( 'wp_ajax_deleteLicense',     array( $this, 'deleteLicense' ) );
    add_action( 'wp_ajax_activateLicense',   array( $this, 'activateLicense' ) );
    add_action( 'wp_ajax_checkLicense',      array( $this, 'checkLicense' ) );
    add_action( 'wp_ajax_deactivateLicense', array( $this, 'deactivateLicense' ) );
    add_action( 'wp_ajax_clearCacheUpdate',  array( $this, 'clearCacheUpdate' ) );
  }

	/**
	   * Save a license key
	   * @return void
	   */
	  public function saveLicense() {

	    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-license-nonce' ) ) {
	      wp_die( esc_html__( 'An error has occurred.', 'wp-translations-pro' ) );
	    }

	    if ( empty( $_POST['license'] )  ) {
	      wp_send_json_error( $data = array( 'message' => esc_html__( 'Please, enter a license key.', 'wp-translations-pro' ) ) );
	    }

	    $name    = $_POST['name'];
	    $slug    = $_POST['slug'];
	    $locale  = $_POST['locale'];
	    $type    = $_POST['type'];
	    $license = $_POST['license'];

	    $this->options['licenses'][ $locale ][ $slug ] = array(
	      'name'    => $name,
	      'license' => trim( $license )
	    );

	    Helper::updateOptions( $this->options );
	    LoggerHelper::log( $license, 'save', $_SERVER, 1 );

	    $data = array(
	      'message' => esc_html__( 'License key successfully saved.', 'wp-translations-pro' ),
	    );
	    wp_send_json_success( $data );

	    die();
	  }

	  /**
	   * Delete a license key
	   * @return void
	   */
	  public function deleteLicense() {

	    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-license-nonce' ) ) {
	      wp_die( esc_html__( 'An error has occurred.', 'wp-translations-pro' ) );
	    }

	    $slug    = $_POST['slug'];
	    $locale  = $_POST['locale'];
	    $license = $_POST['license'];

	    if ( LicenseHelper::isValid( $slug, $locale ) ) {
	      $api   = EDD_SL_Api::deactivateLicense( $slug, $locale );
	      LoggerHelper::log( $license, 'deactivate', $_SERVER, $api );
	    }
	    unset( $this->options['licenses'][ $locale ][ $slug ] );

	    Helper::updateOptions( $this->options );
	    delete_site_transient( 'wpt_license_' . $locale . '_' . $slug );

	    LoggerHelper::log( $license, 'delete', $_SERVER, 1 );
	    wp_clean_update_cache();
	    wp_send_json_success( $data = array( 'message' => esc_html__( 'License key successfully deleted.', 'wp-translations-pro' ) ) );

	    die();
	  }

	  /**
	   * Activate a license key
	   * @return void
	   */
	  public function activateLicense() {

	    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-license-nonce' ) ) {
	      wp_die( esc_html__( 'An error has occurred.', 'wp-translations-pro' ) );
	    }

	    $slug    = $_POST['slug'];
	    $locale  = $_POST['locale'];
	    $license = $_POST['license'];
	    $type    = $_POST['type'];
	    $api     = EDD_SL_Api::activateLicense( $slug, $locale );

	    $this->options['licenses'][ $locale ][ $slug ]['data'] = $api;

	    if( true === $api->success && 'valid' == $api->license ) {

	      Helper::updateOptions( $this->options );
	      LoggerHelper::log( $license, 'activate', $_SERVER, $api );

				$version = EDD_SL_API::getVersion( $slug, $locale );

	      $data = array(
	        'api'     => $api,
					'version' => $version,
	        'message' => esc_html__( 'License successfully activated.', 'wp-translations-pro' ),
	        'updates' => wp_get_translation_updates(),
	      );
	      wp_send_json_success( $data );

	    } else {

	      if ( isset( $api->error ) && 'no_activations_left' == $api->error || isset( $api->error ) && 'expired' == $api->error ) {
	        $productName = $this->products[ $locale ][ $slug ]['title'];
	        if( urldecode( $api->license_download ) != $productName ) {
	          $api->error   = 'item_name_mismatch';
	        }
	      }

	      Helper::updateOptions( $this->options );
	      LoggerHelper::log( $license, 'activate', $_SERVER, $api );

	      $data = array(
	        'api'     => $api,
	        'message' => LicenseHelper::messageStatus( $api ),
	      );
	      wp_send_json_error( $data );
	    }

	    die();
	  }

	  /**
	   * Check a license key
	   * @return void
	   */
	  public function checkLicense() {

	    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-license-nonce' ) ) {
	      wp_die( esc_html__( 'An error has occurred.', 'wp-translations-pro' ) );
	    }

	    $slug    = $_POST['slug'];
	    $locale  = $_POST['locale'];
	    $api     = EDD_SL_Api::checkLicense( $slug, $locale );

	    $this->options['licenses'][ $locale ][ $slug ]['data'] = $api;
	    Helper::updateOptions( $this->options );

	    die();
	  }

	  /**
	   * Deactivate a license key
	   * @return void
	   */
	  public function deactivateLicense() {

	    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-license-nonce' ) ) {
	      wp_die( esc_html__( 'An error has occurred.', 'wp-translations-pro' ) );
	    }

	    $slug    = $_POST['slug'];
	    $locale  = $_POST['locale'];
	    $type    = $_POST['type'];
	    $license = $_POST['license'];
	    $api     = EDD_SL_Api::deactivateLicense( $slug, $locale );

	    LoggerHelper::log( $license, 'deactivate', $_SERVER, $api );

	    if( true === $api->success && 'deactivated' == $api->license ) {

	      delete_site_transient( 'wpt_license_' . $locale . '_' . $slug );
	      unset( $this->options['licenses'][ $locale ][ $slug ]['data'] );
	      Helper::updateOptions( $this->options );
	      Helper::clearUpdateCache( $type );

	      $data = array(
	        'api' => $api,
	        'message' => esc_html__( 'License successfully deactivated.', 'wp-translations-pro' )
	      );
	      wp_send_json_success( $data );

	    } else {
	      $data = array(
	        'api' => $api,
	        'message' => esc_html__( 'License fails to be deactivated.', 'wp-translations-pro' )
	      );
	      wp_send_json_error( $data );
	    }

	    die();
	  }

	  public function checkAllLicense() {

	    if( ! empty( $this->options['licenses'] ) ) {
	      foreach ( $this->options['licenses'] as $locale => $product ) {
	        foreach( $product as $slug => $license ) {

	          $cached = get_site_transient( 'wpt_license_' . $locale .'_' . $slug );
	          if ( false === $cached ) {

	            $data = array(
	              'slug' => $slug,
	              'locale' => $locale
	            );
	            $this->licenseBackground->data( $data )->dispatch();

	          }
	        }
	      }
	    }
	  }

	  /**
	   * Clear update cache
	   * @return void
	   */
	  public function clearCacheUpdate() {

	    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-license-nonce' ) ) {
	      wp_die( esc_html__( 'An error has occurred.', 'wp-translations-pro' ) );
	    }

	    $type    = trim( $_POST['type'] );

	    Helper::clearUpdateCache( $type );

	    $data = array(
	      'message' => esc_html__( 'Check for updates done.', 'wp-translations-pro' ),
	      'updates' => wp_get_translation_updates(),
	    );
	    wp_send_json_success( $data );

	    die();
	  }


}
