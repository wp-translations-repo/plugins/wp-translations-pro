<?php

namespace WP_Translations_Pro\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\Models\HooksAdminInterface;
use WP_Translations_Pro\WordPress\Helpers\Helper;
use WP_Translations_Pro\WordPress\Helpers\ProductHelper;
use WP_Translations_Pro\WordPress\Helpers\LicenseHelper;
use WP_Translations_Pro\WordPress\Helpers\LoggerHelper;
use WP_Translations_Pro\WordPress\Helpers\TranslationHelper;
use WP_Translations_Pro\APIs\EDD_SL_Api;

/**
 * Translations Updater
 *
 * @since 1.0.0
 */

class TranslationUpdater implements HooksAdminInterface {

  public function hooks() {
    add_filter( 'pre_set_site_transient_update_plugins',  array( $this, 'pre_set_site_transient' ) );
    add_filter( 'http_request_args',                      array( $this, 'preempt_expect_header' ) );
  }

  public function pre_set_site_transient( $transient ) {

    if ( ! is_object( $transient ) ) {
      $transient = new \stdClass();
    }

    require_once( ABSPATH . 'wp-admin/includes/translation-install.php' );

    $options  = Helper::getOptions();
    $products = ProductHelper::localProducts();

    if ( ! empty( $options['licenses'] ) ) {
      foreach ( $options['licenses'] as $locale => $product ) {
        foreach( $product as $slug => $license ) {

          if ( false !== LicenseHelper::isValid( $slug, $locale ) ) {

            $translations = wp_get_installed_translations( $products[ $locale ][ $slug ]['type'] . 's' );
            $updater      = EDD_SL_Api::getVersion( $slug, $locale );
            $updateSlug   = TranslationHelper::rewriteTextdomain( $slug );

            $remotePoRevision   = isset( $updater->po_revision ) ? strtotime( $updater->po_revision ) : 0;
            $localPoRevision    = TranslationHelper::getLocalPoRevisionDate( $translations, $updateSlug, $locale );

            if ( $remotePoRevision > $localPoRevision ) {
              $update = array(
                'type'            => $products[ $locale ][ $slug ]['type'],
                'slug'            => $updateSlug,
                'language'        => $locale,
                'version'         => trim( $updater->new_version ),
                'updated'         => $updater->last_updated,
                'package'         => $updater->package,
                'autoupdate'      => 1,
                'po_version'      => $updater->po_revision,
                'localRevision'   => $localPoRevision,
                'remoteRevision'  => $remotePoRevision,

              );

              $transient->translations[] = $update;
              $version = array( 'product' => $updater->new_version, 'po_revision' => strtotime( $updater->po_revision ) );
              LoggerHelper::log( $slug . '-' . $locale, 'update-available', $_SERVER, 1, $version );
            }
          }
        }
      }

    }

    return $transient;

  }

  public function preempt_expect_header( $r ) {
  	$r['headers']['Expect'] = '';
  	return $r;
  }

}
