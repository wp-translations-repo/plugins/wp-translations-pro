<?php

namespace WP_Translations_Pro\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\Models\HooksAdminInterface;
use WP_Translations_Pro\WordPress\Helpers\Helper;
use WP_Translations_Pro\WordPress\Helpers\ProductHelper;
use WP_Translations_Pro\WordPress\Helpers\TranslationHelper;
use WP_Translations_Pro\WordPress\Admin\TranslationNotification;

/**
 * Enqueue Styles and Scripts
 *
 * @since 1.0.0
 */

class EnqueueAssets implements HooksAdminInterface {

  /**
   * @see WP_Translations_Pro\Models\HooksInterface
   */
  public function hooks() {
    add_action( 'admin_enqueue_scripts', array( $this, 'enqueueStyles' ) );
    add_action( 'admin_enqueue_scripts', array( $this, 'enqueueScripts' ) );
    add_action( 'admin_enqueue_scripts', array( $this, 'inlineStyles' ) );
  }

  public function enqueueStyles() {
    $options = Helper::getOptions();
    $screen  = get_current_screen();
    $css_ext = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '.css' : '.min.css';

    wp_register_style(
      'wptpro-admin-styles',
      WPTPRO_PLUGIN_URL . 'assets/css/wpt-styles' . $css_ext,
      array(),
      WPTPRO_VERSION
    );

    wp_register_style(
      'wptpro-datatables',
      WPTPRO_PLUGIN_URL . 'assets/css/jquery-datatables' . $css_ext,
      array(),
      '1.10.16'
    );

    if ( in_array( $screen->base, Helper::assetsScreen() ) ) {
      wp_enqueue_style( 'wptpro-admin-styles' );
      wp_enqueue_style( 'wptpro-datatables' );
    }

    $updateCoreScreen = is_multisite() ? 'update-core-network' : 'update-core';

    if ( isset( $screen ) && $updateCoreScreen === $screen->id && false !== (bool) $options['settings']['core_updates'] ) {
      wp_enqueue_style( 'wptpro-admin-styles' );
      wp_enqueue_style( 'wptpro-datatables' );
    }

  }

  public function enqueueScripts() {

    $screen         = get_current_screen();
    $options        = Helper::getOptions();
    $products       = ProductHelper::localProducts();
    $storesData     = ProductHelper::STORES;
    $js_ext         = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '.js' : '.min.js';
    $currentLocale  = get_user_locale();
		$migrationInstall = get_option( 'wpt_pro_migration_install' );

    $themesTranslations = array();
    $themesUpdates = wp_get_translation_updates();

    foreach ( $themesUpdates as $theme ) {
      if( 'theme' == $theme->type ) {
        $themesTranslations[ $theme->slug ]['updates'][] = $theme->language;
      }
    }

    $allThemes         = wp_get_themes();
    $themesProducts    = array();
    foreach ( $allThemes as $theme ) {

      $themeSlug = ( ! empty( $theme->get( 'TextDomain' ) ) ) ? $theme->get( 'TextDomain' ) : TranslationHelper::sanitizeTextdomain( $theme->get( 'Name' ) );
      if ( false !== ProductHelper::isToPromote( $themeSlug, $currentLocale ) ) {
        $themesProducts[ $themeSlug ] = $themeSlug;
      }

    }

		$datatableTranslations = array(
      'sEmptyTable' 		=> esc_html__( 'No data available', 'wp-translations-pro' ),
      'sInfo' 					=> esc_html_x( 'Showing _START_ to _END_ of _TOTAL_ entries', 'Please don\'t translate: _START_ _END_ _TOTAL_', 'wp-translations-pro' ),
      'sInfoEmpty' 			=> esc_html__( 'Showing 0 to 0 of 0 entries', 'wp-translations-pro' ),
      'sInfoFiltered' 	=> esc_html_x( 'filtered from _MAX_ total entries', 'Please don\'t translate: _MAX_', 'wp-translations-pro' ),
      'sLengthMenu' 		=> esc_html_x( 'Show _MENU_ entries', 'Please don\'t translate: _MENU_', 'wp-translations-pro' ),
      'sLoadingRecords' => esc_html__( 'Processing...', 'wp-translations-pro' ),
      'sSearch' 				=> esc_html__( 'Search:', 'wp-translations-pro' ),
      'sZeroRecords' 		=> esc_html__( 'No matching records found', 'wp-translations-pro' ),
      'sFirst' 					=> esc_html__( 'First', 'wp-translations-pro' ),
      'sLast' 					=> esc_html__( 'Last', 'wp-translations-pro' ),
      'sNext' 					=> esc_html__( 'Next', 'wp-translations-pro' ),
      'sPrevious' 			=> esc_html__( 'Previous', 'wp-translations-pro' ),
      'sSortAscending' 	=> esc_html__( ': activate to sort column ascending', 'wp-translations-pro' ),
      'sSortDescending' => esc_html__( ': activate to sort column descending', 'wp-translations-pro' ),
    );


    $wpt_license_data = array(
      'ajaxurl'        => admin_url( 'admin-ajax.php' ),
      'nonce'          => wp_create_nonce( 'wpt-license-nonce' ),
      'update_nonce'   => wp_create_nonce( 'wpt-update-nonce' ),
      'stores'         => $storesData,
      'products'       => ProductHelper::localProducts(),
      'i18n'           => array(
        'btn'       => array(
          'save'            => esc_html__( 'Save License', 'wp-translations-pro' ),
          'deleted'         => esc_html__( 'Delete License', 'wp-translations-pro' ),
          'deleted_plugin'  => esc_html__( 'Delete Plugin', 'wp-translations-pro' ),
          'activate'        => esc_html__( 'Activate', 'wp-translations-pro' ),
          'deactivate'      => esc_html__( 'Deactivate', 'wp-translations-pro' ),
          'renew'           => esc_html__( 'Renew', 'wp-translations-pro' ),
          'upgrade'         => esc_html__( 'Upgrade', 'wp-translations-pro' ),
          'download'        => esc_html__( 'Install translations', 'wp-translations-pro' ),
          'done'            => esc_html__( 'Activated and installed', 'wp-translations-pro' ),
          'configure'       => esc_html__( 'Configure', 'wp-translations-pro' ),
					'next'            => esc_html__( 'Next Step', 'wp-translations-pro' ),
        ),
        'status'    => array(
          'expired'         => esc_html__( 'License expired', 'wp-translations-pro' ),
          'activated'       => esc_html__( 'License activated', 'wp-translations-pro'),
          'can_activated'   => esc_html__( 'License can be activated', 'wp-translations-pro'),
          'deactivated'     => esc_html__( 'License deactivated', 'wp-translations-pro' ),
          'unlimited'       => esc_html__( 'unlimited', 'wp-translations-pro' ),
        ),
        'messages'  => array(
          'check_license'   => esc_html__( 'Checking license status', 'wp-translations-pro' ),
          'download_lp'     => esc_html__( 'Downloading and installing language pack', 'wp-translations-pro' ),
          'check_update'    => esc_html__( 'Checking for new updates', 'wp-translations-pro' ),
          'clear_cache'     => esc_html__( 'Clearing update cache', 'wp-translations-pro' ),
          'lp_installed'    => esc_html__( 'Language pack installed!', 'wp-translations-pro' ),
          'deleting'        => esc_html__( 'Deleting license', 'wp-translations-pro' ),
          'saving'          => esc_html__( 'Saving license', 'wp-translations-pro' ),
          'deactivating'    => esc_html__( 'Deactivating license', 'wp-translations-pro' ),
					'deleting_plugin' => esc_html__( 'Deleting old plugin', 'wp-translations-pro' ),
					'deleted_plugin'  => esc_html__( 'Old plugin deleted', 'wp-translations-pro' ),
        ),
        'datatable' => $datatableTranslations,
      ),
    );

		$wpt_update_core = array(
      'ajaxurl'             => admin_url( 'admin-ajax.php' ),
      'nonce'               => wp_create_nonce( 'wpt-update-nonce' ),
      'updating_message'    => esc_html__( 'Updating translations', 'wp-translations-pro' ),
      'updated_message'     => esc_html__( 'Translations updated', 'wp-translations-pro' ),
      'all_updated_message' => esc_html__( 'The translations are up to date.', 'wp-translations-pro' ),
      'i18n' => array(
        'messages'  => array(
          'download_lp'     => esc_html__( 'Downloading and installing your language pack', 'wp-translations-pro' ),
          'check_update'    => esc_html__( 'Check for updates', 'wp-translations-pro' ),
          'clear_cache'     => esc_html__( 'Clearing cache', 'wp-translations-pro' ),
          'lp_installed'    => esc_html__( 'Language pack installed!', 'wp-translations-pro' ),
        ),
        'datatable'         => $datatableTranslations,
      ),
    );


    $wpt_update_ajax = array(
      'ajaxurl'             => admin_url( 'admin-ajax.php' ),
      'nonce'               => wp_create_nonce( 'wpt-update-nonce' ),
      'plugins_updates'     => (bool) $options['settings']['plugins_updates'],
      'themes_updates'      => (bool) $options['settings']['themes_updates'],
      'themes_translations' => $themesTranslations,
      'themes_products'     => $themesProducts,
      'update_message'      => esc_html__( 'New translations are available:&nbsp;', 'wp-translations-pro' ),
      'update_button'       => esc_html__( 'Update now' , 'wp-translations-pro' ),
      'promote_message'     => esc_html__( 'A premium translation is available in our store!', 'wp-translations-pro' ),
			'view_details'        => esc_html__( 'View details', 'wp-translations-pro' ),
    );

    wp_register_script(
      'wptpro-admin-scripts',
      WPTPRO_PLUGIN_URL . 'assets/js/wpt-scripts' . $js_ext,
      array( 'jquery' ),
      WPTPRO_VERSION,
      true
    );

    wp_register_script(
      'wptpro-wizard-scripts',
      WPTPRO_PLUGIN_URL . 'assets/js/wpt-wizard' . $js_ext,
      array( 'jquery' ),
      WPTPRO_VERSION,
      true
    );

    wp_register_script(
      'wptpro-tabs',
      WPTPRO_PLUGIN_URL . 'assets/js/jquery-accessible-tabs-aria' . $js_ext,
      array( 'jquery' ),
      '1.6.1',
      true
    );

    wp_register_script(
      'wptpro-modal',
      WPTPRO_PLUGIN_URL . 'assets/js/jquery-accessible-modal-window-aria' . $js_ext,
      array( 'jquery' ),
      '1.9.2',
      true
    );

    wp_register_script(
      'wptpro-hide-show',
      WPTPRO_PLUGIN_URL . 'assets/js/jquery-accessible-hide-show-aria' . $js_ext,
      array( 'jquery' ),
      '1.8.0',
      true
    );

    wp_register_script(
      'wptpro-update-core',
      WPTPRO_PLUGIN_URL . 'assets/js/wpt-update-core' . $js_ext,
      array( 'jquery' ),
      WPTPRO_VERSION,
      true
    );

    wp_register_script(
      'wptpro-notifications',
      WPTPRO_PLUGIN_URL . 'assets/js/wpt-notifications' . $js_ext,
      array( 'jquery' ),
      WPTPRO_VERSION,
      true
    );

    wp_register_script(
      'wptpro-datatables',
      WPTPRO_PLUGIN_URL . 'assets/js/jquery-datatables' . $js_ext,
      array( 'jquery' ),
      '1.10.16',
      true
    );

    if ( in_array( $screen->base, Helper::assetsScreen() ) && false === $migrationInstall ) {
			wp_enqueue_script( 'wptpro-wizard-scripts' );
      wp_localize_script( 'wptpro-wizard-scripts', 'wpt_license_ajax', $wpt_license_data );
    }

    if ( in_array( $screen->base, Helper::assetsScreen() ) ) {
      wp_enqueue_script( 'wptpro-tabs' );
      wp_enqueue_script( 'wptpro-modal' );
      wp_enqueue_script( 'wptpro-hide-show' );
      wp_enqueue_script( 'wptpro-datatables' );
      wp_enqueue_script( 'wptpro-admin-scripts' );
      wp_localize_script( 'wptpro-admin-scripts', 'wpt_license_ajax', $wpt_license_data );
    }

    $updateCoreScreen = is_multisite() ? 'update-core-network' : 'update-core';

    if ( isset( $screen ) && $updateCoreScreen === $screen->id && false !== (bool) $options['settings']['core_updates'] ) {
			wp_enqueue_script( 'wptpro-datatables' );
      wp_enqueue_script( 'wptpro-update-core' );
			wp_localize_script( 'wptpro-update-core', 'wpt_update_core', $wpt_update_core );
      wp_enqueue_script( 'wptpro-tabs' );
      wp_enqueue_script( 'wptpro-modal' );

    }

    if ( 'plugins' == $screen->id && false !== (bool) $options['settings']['plugins_updates'] || 'themes' == $screen->id && false !== (bool) $options['settings']['themes_updates'] ) {
      wp_enqueue_script( 'wptpro-notifications' );
      wp_localize_script( 'wptpro-notifications', 'wpt_update_ajax', $wpt_update_ajax );
    }

  }

  function inlineStyles() {
    wp_enqueue_style( 'admin-menu' );

    $custom_css = "
      #adminmenu #toplevel_page_wp-translations-pro div.wp-menu-image img {
          padding-top: 5px;
          max-width: 24px;
      }";
    wp_add_inline_style( 'admin-menu', $custom_css );
  }

}
