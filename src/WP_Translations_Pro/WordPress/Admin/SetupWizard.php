<?php

namespace WP_Translations_Pro\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\Models\HooksAdminInterface;
use WP_Translations_Pro\WordPress\Helpers\ProductHelper;
use WP_Translations_Pro\WordPress\Helpers\LicenseHelper;
use WP_Translations_Pro\WordPress\Helpers\WizardHelper;

/**
 * Setup Wizard
 *
 * @since 1.1.6
 */

class SetupWizard implements HooksAdminInterface {

	public function hooks() {
    add_action( 'wp_ajax_stepMigrate',    array( $this, 'migrateLicense' ) );
    add_action( 'wp_ajax_deletePlugin',   array( $this, 'deletePlugin' ) );
	}

	public function migrate() {

		$errors           = array();
		$plugins          = get_plugins();
		$pluginsToDestroy = ProductHelper::oldProducts();
		$products         = ProductHelper::localProducts();
		$locale           = get_user_locale();

		foreach( $plugins as $key => $plugin ) {
			$productSlug = str_replace( '-french', '', $plugin['TextDomain'] );
			if ( in_array( $key, $pluginsToDestroy ) ) {
				$errors[ $productSlug ]              = $plugin;
				$errors[ $productSlug ]['license']   =  str_replace( '-', '_', $plugin['TextDomain'] ) . '_license_key';
				$errors[ $productSlug ]['product']   = $productSlug;
				$errors[ $productSlug ]['name']      = $plugin['Name'];
				$errors[ $productSlug ]['type']      = 'plugins';
				$errors[ $productSlug ]['lang']      = $locale;
				$errors[ $productSlug ]['path']      = $key;
			}

		}


			if ( ! empty( $errors ) ) :  ?>
				<div class="wp-translations-notice">
					<p><?php esc_html_e( 'To avoid conflicts we automaticly delete older plugins after migrate licenses keys.', 'wp-translations-pro' ); ?></p>
				</div>


			<table class="wpt-table wpt-plugins-conflict" style="width: 100%">
				<thead>
					<tr>
						<th><?php esc_html_e( 'Plugins', 'wp-translations-pro' ); ?></th>
						<th><?php esc_html_e( 'Licenses', 'wp-translations-pro' ); ?></th>
						<th class="column-action"><?php esc_html_e( 'Actions', 'wp-translations-pro' ); ?></th>
					</tr>
				</thead>
			<?php	foreach( $errors as $key => $error ) : ?>

				<tr id="wpt-row-license-<?php echo esc_attr( $key ); ?>-<?php echo get_user_locale(); ?>">
					<td><strong><?php echo esc_html( $error['name'] ); ?></strong></td>
					<td>
						<?php if (  false !== get_site_option( $error['license'] ) ) : ?>
							<?php echo get_site_option( $error['license'] ); ?>
						<?php endif; ?>
					</td>
					<td id="wpt-col-actions-<?php echo get_user_locale(); ?>-<?php echo esc_attr( $key ); ?>" class="column-action">
							<?php if (  get_site_option( $error['license'] ) ) : ?>
							<button
								id="wpt-migrate-<?php echo esc_attr( $error['product'] ); ?>-<?php echo esc_attr( $locale ); ?>"
								class="wpt-button primary wpt-migrate-actions"
								data-name="<?php echo esc_html( $error['Name'] ); ?>"
								data-type="<?php echo esc_attr(  $error['type'] ); ?>"
								data-key="<?php echo $error['path']; ?>"
								data-slug="<?php echo $error['product']; ?>"
								data-locale="<?php echo $error['lang']; ?>"
								data-license="<?php echo get_site_option( $error['license'] ); ?>"><?php esc_html_e( 'Migrate license', 'wp-translations-pro' ); ?></button>
							<?php else: ?>
								<button class="wpt-button danger wpt-migrate-actions"
												id="wpt-delete-plugin-<?php echo $error['product']; ?>"
												data-key="<?php echo $error['path']; ?>"
												data-slug="<?php echo $error['product']; ?>"
												data-locale="<?php echo $locale; ?>"><?php esc_html_e( 'Delete old plugin' ); ?></button>
							<?php endif; ?>
						</td>
				</tr>
				<tr id="wpt-row-notice-<?php echo $locale; ?>-<?php echo esc_attr( $key ); ?>" class="wpt-license-notice hidden">

				</tr>
			<?php endforeach; ?>
		</table>
		<?php else : ?>
			<?php
			 	update_site_option( 'wpt_pro_step', '2' );
				esc_html_e( 'Nothing to migrate', 'wp-translations-pro' ); ?>
		<?php endif;
		die();
	}


	public function deletePlugin() {

		global $wp_filesystem;
		$errors = get_site_option( 'wpt_pro_pending_migration' );


		$slugs = array( $_POST['plugin'] );
		$slug  = $_POST['slug'];

		$deactivation = deactivate_plugins( $slugs );
		$result       = delete_plugins( $slugs );

		unset( $errors[ $slug ] );

		if( ! empty( $errors) ) {
			update_site_option( 'wpt_pro_pending_migration', $errors );
		} else {
			delete_site_option( 'wpt_pro_pending_migration' );
		}

		if ( is_wp_error( $result ) ) {
        $status['errorMessage'] = $result->get_error_message();
				unset( $errors[ $slug ] );
				update_site_option( 'wpt_pro_pending_migration', $errors );
        wp_send_json_error( $errors );
    } elseif ( false === $result ) {
        $status['errorMessage'] = __( 'Plugin could not be deleted.' );
        wp_send_json_error( $errors );
    } else {
			wp_send_json_success();
		}

		die();
	}

}
