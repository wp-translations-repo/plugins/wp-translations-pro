<?php

namespace WP_Translations_Pro\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\Models\HooksAdminInterface;
use WP_Translations_Pro\WordPress\Helpers\Helper;
use WP_Translations_Pro\WordPress\Helpers\ProductHelper;

/**
 * settings Actions
 *
 * @since 1.0.0
 */

class settingsActions implements HooksAdminInterface {

  /**
   * @see WP_Translations_Pro\Models\HooksInterface
   */
  public function hooks() {
    add_action( 'admin_init',           array( $this, 'processActions' ) );
    add_action( 'wpt_pro_saveSettings', array( $this, 'wpt_pro_saveSettings' ) );
  }

  public function processActions() {

    if ( isset( $_POST['wpt-pro-action'] ) ) {
      do_action( 'wpt_pro_' . $_POST['wpt-pro-action'], $_POST );
    }

    if ( isset( $_GET['wpt-pro-action'] ) ) {
      do_action( 'wpt_pro_' . $_GET['wpt-pro-action'], $_GET );
    }

  }

  /**
   * Saves settings
   *
   * @since 1.0
   * @param array $data settings post data
   * @return void
   */
  function wpt_pro_saveSettings( $data ) {

    if ( ! isset( $data['wp-pro-settings-nonce'] ) || ! wp_verify_nonce( $data['wp-pro-settings-nonce'], 'wpt_pro_settings_nonce' ) ) {
      wp_die( esc_html__( 'Trying to cheat or something?', 'wp-translations-pro' ), esc_html__( 'Error', 'wp-translations-pro' ), array( 'response' => 403 ) );
    }

    $options = Helper::getOptions();

    if ( isset( $data['wpt_pro_settings']['force_products'] ) ) {

      $stores = ProductHelper::STORES;

      foreach ( $stores as $locale => $store ) {
        $products = ProductHelper::localProducts();
        foreach ( $products as $locale => $product ) {
          foreach( $product as $slug => $translation ) {
            delete_site_transient( 'wpt_readme_' . $slug . '_' . $locale );
          }
        }
        delete_site_transient( 'wpt_edd_products_' . $locale );
      }

      $message = '&wpt-message=products_reloaded';

    } elseif ( isset( $data['wpt_pro_settings']['force_translations'] ) ) {

      delete_site_transient( 'update_plugins' );
      delete_site_transient( 'update_themes' );
      delete_site_transient( 'update_core' );
      $message = '&wpt-message=check_translations_updates';

    } elseif ( isset( $data['wpt_pro_settings']['purge_logs'] ) ) {

      delete_site_option( 'wpt_pro_logs' );
      $message = '&wpt-message=purge_logs';

    } else {

      $options['settings']['core_updates']    = isset( $data['wpt_pro_settings']['core_updates'] ) ? $data['wpt_pro_settings']['core_updates'] : '';
      $options['settings']['plugins_updates'] = isset( $data['wpt_pro_settings']['plugins_updates'] ) ? $data['wpt_pro_settings']['plugins_updates'] : '';
      $options['settings']['themes_updates']  = isset( $data['wpt_pro_settings']['themes_updates'] ) ? $data['wpt_pro_settings']['themes_updates'] : '';
      $options['settings']['bubble_count']    = isset( $data['wpt_pro_settings']['bubble_count'] ) ? $data['wpt_pro_settings']['bubble_count'] : '';
      $options['settings']['page_hook']       = isset( $data['wpt_pro_settings']['page_hook'] ) ? $data['wpt_pro_settings']['page_hook'] : '';
      $options['settings']['debug']           = isset( $data['wpt_pro_settings']['debug'] ) ? $data['wpt_pro_settings']['debug'] : '';

      Helper::updateOptions( $options );
      $message = '&wpt-message=settings_updated';
    }

    $pageRedirect = ( 'menu' == $options['settings']['page_hook'] ) ? 'admin.php' : 'options-general.php';
    wp_redirect( admin_url( $pageRedirect . '?page=wp-translations-pro&wpt-page=settings' . $message ) );

  }

}
