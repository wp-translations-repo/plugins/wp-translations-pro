<?php

namespace WP_Translations_Pro\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\Models\HooksAdminInterface;


class i18n implements HooksAdminInterface {

	public function hooks() {
			add_action( 'init', array( $this, 'translate' ) );
	}

	public function translate() {
		load_plugin_textdomain( "wp-translations-pro", false, dirname( WPTPRO_BASE_FILE ) . '/languages' );
	}

}
