<?php

namespace WP_Translations_Pro\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\Models\HooksAdminInterface;
use WP_Translations_Pro\WordPress\Helpers\LoggerHelper;
use WP_Translations_Pro\WordPress\Helpers\Helper;
use WP_Translations_Pro\WordPress\Helpers\FileHelper;
use WP_Translations_Pro\WordPress\Helpers\ProductHelper;
use WP_Translations_Pro\WordPress\Helpers\TranslationHelper;

/**
 * Upgrade Translations
 *
 * @since 1.1.2
 */

class TranslationUpgrader implements HooksAdminInterface {

  /**
   * @see WP_Translations_Pro\Models\HooksInterface
   */
  public function hooks() {
    add_action( 'wp_ajax_upgradeTranslation',  array( $this, 'upgradeTranslation' ),-1 );
    add_action( 'wp_ajax_installTranslations', array( $this, 'installTranslations' ),-1 );
    add_action( 'upgrader_process_complete',   array( $this, 'upgradeComplete' ),10 , 2 );
    add_action( 'current_screen',              array( $this, 'clearUpdateCache'), -1 );
  }

  public function upgradeTranslation() {

    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-update-nonce' ) ) {
      wp_die( esc_html__( 'You don&#8217;t have permission to do this.', 'wp-translations-pro' ) );
    }

    $status = array(
      'update' => 'translations',
    );
    if ( ! current_user_can( 'update_core' ) && ! current_user_can( 'update_plugins' ) && ! current_user_can( 'update_themes' ) ) {
      $status['errorMessage'] = __( 'You do not have sufficient permissions to update this site.', 'wp-translations-pro' );
      wp_send_json_error( $status );
    }

    $slug       = esc_attr( $_POST['slug'] );
    $type       = esc_attr( $_POST['type'] );
    $locale     = explode( '|', $_POST['locale'] );

    include_once( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );

    $allLanguageUpdates = wp_get_translation_updates();

    $languageUpdates = array();
    foreach ( $allLanguageUpdates as $currentLanguageUpdate ) {
      if ( $currentLanguageUpdate->slug == $slug && in_array( $currentLanguageUpdate->language, $locale ) ) {
        $languageUpdates[] = $currentLanguageUpdate;
      }
    }

    $status['lp'] = $languageUpdates;

    $skin     = new \Automatic_Upgrader_Skin();
    $upgrader = new \Language_Pack_Upgrader( $skin );
    $result   = $upgrader->bulk_upgrade( $languageUpdates, array( 'clear_update_cache' => false ) );

    $status['message'] = $upgrader->skin->get_upgrade_messages();

    if ( is_array( $result ) && is_wp_error( $skin->result ) ) {
      $result = $skin->result;
    }

    if ( ( is_array( $result ) && ! empty( $result[0] ) ) || true === $result ) {

      $status['clearCache'] = set_site_transient( 'wpt_clear_cache', '1' );
      wp_send_json_success( $status );

    } elseif ( is_wp_error( $result ) ) {

      $status['errorMessage'] = $result->get_error_message();
      wp_send_json_error( $status );

    } elseif ( false === $result ) {

      global $wp_filesystem;
      $status['errorCode']    = 'unable_to_connect_to_filesystem';
      $status['errorMessage'] = __( 'Unable to connect to the filesystem. Please confirm your credentials.', 'wp-translations-pro' );
      // Pass through the error from WP_Filesystem if one was raised.
      if ( $wp_filesystem instanceof WP_Filesystem_Base && is_wp_error( $wp_filesystem->errors ) && $wp_filesystem->errors->get_error_code() ) {
        $status['errorMessage'] = esc_html( $wp_filesystem->errors->get_error_message() );
      }

      wp_send_json_error( $status );

    }
    // An unhandled error occurred.
    $status['errorMessage'] = __( 'Translations update failed.', 'wp-translations-pro' );
    wp_send_json_error( $status );

  }

  public function upgradeComplete( $upgrader_object, $options ) {

    if ( 'update' == $options['action'] && 'translation' == $options['type']  ) {

      require_once( ABSPATH . 'wp-admin/includes/translation-install.php' );

      foreach( $options['translations'] as $update ) {

        $translations = wp_get_installed_translations( $update['type'] . 's' );
        $updateSlug   = TranslationHelper::rewriteTextdomain( $update['slug'] );
        $po_revision  = TranslationHelper::getLocalPoRevisionDate( $translations, $updateSlug, $update['language'] );
        $version      = array( 'product' => $update['version'], 'po_revision' => $po_revision );
        LoggerHelper::log( $update['slug'] . '-' . $update['language'], 'update', $_SERVER, 1, $version );

        if ( is_dir(  WPTPRO_CONTENT_PATH_TEMP . '/' . $update['slug'] )  ) {
          FileHelper::removeRecursiveDir( WPTPRO_CONTENT_PATH_TEMP . '/' . $update['slug'] );
        }
      }
    }
  }

  public function clearUpdateCache() {

    $screen  = get_current_screen();
    $screens = array(
      'plugins',
      'themes',
      'update-core'
    );

    if ( in_array( $screen->base, $screens ) && false !== get_site_transient( 'wpt_clear_cache' ) ) {
      Helper::generateUpdateCache();
      delete_site_transient( 'wpt_clear_cache' );
    }
  }

	public function installTranslations() {

		if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-license-nonce' ) ) {
			wp_die( esc_html__( 'An error has occurred.', 'wp-translations-pro' ) );
		}
		global $wp_filesystem;

		$url     = esc_url( $_POST['url'] );
		$type    = $_POST['type'];
		$slug    = $_POST['slug'];
		$locale  = $_POST['locale'];

		$zipPath    = WPTPRO_CONTENT_PATH_TEMP . '/' . $slug . '-' . $locale . '.zip';
		$zipContent = download_url( $url, $timeout  = 300 );
		copy( $zipContent, $zipPath );
		$translations = unzip_file( $zipPath, WP_LANG_DIR . '/' . $type . '/' );
		$data         = array( 'url' => $url, 'download' => $zipContent, 'unzip' => $translations );

		unlink( $zipPath );

		Helper::clearUpdateCache( $type );

    if ( true === $translations ) {
			$data['message'] = esc_html__( 'Language pack installed!', 'wp-translations-pro' );
      wp_send_json_success( $data );
		} else {
			$data['message'] = esc_html__( 'Something went wrong with the archive.', 'wp-translations-pro' );
      wp_send_json_error( $data );
		}

		die();
	}

}
