<?php

namespace WP_Translations_Pro\WordPress\Cron;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations_Pro\Models\HooksInterface;

/**
 * Purge logs daily
 *
 * @since 1.1.5
 */

class LogCron implements HooksInterface {

	public function hooks() {
		add_action( 'wpt_pro_purge_logs', array( $this, 'purgeLogs' ) );
		add_filter( 'cron_schedules',     array( $this, 'AddIntervals' ) );

	}

	public function addIntervals( $schedules ) {

		$schedules['weekly'] = array(
			'interval' => 604800,
			'display'  => __( 'Once Weekly', 'wp-translations-pro' )
		);
		$schedules['monthly'] = array(
			'interval' => 2635200,
			'display'  => __( 'Once a month', 'wp-translations-pro' )
		);

		return $schedules;
	}

	public function purgeLogs() {
		delete_site_option( 'wpt_pro_logs' );
	}

}
