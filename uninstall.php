<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @author     WP-Translations Team
 * @link       https://wp-translations.pro
 * @since      1.0.3
 *
 * @package    WP-Translations Pro
 */

// Exit if accessed directly.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) exit;

global $wpdb;

function removeRecursiveDir( $dir ) {
	if ( ! is_dir( $dir ) ) {
		// @codingStandardsIgnoreStart
		unlink( $dir );
		// @codingStandardsIgnoreEnd
		return;
	}
	if ( $globs = glob( $dir . '/*', GLOB_NOSORT ) ) {
		foreach ( $globs as $file ) {
			// @codingStandardsIgnoreStart
			is_dir( $file ) ? removeRecursiveDir( $file ) : unlink( $file );
			// @codingStandardsIgnoreEnd
		}
	}
	// @codingStandardsIgnoreStart
	rmdir( $dir );
	// @codingStandardsIgnoreStart
}

// Options
delete_site_option( 'wpt_pro_version' );
delete_site_option( 'wpt_pro_settings' );
delete_site_option( 'wpt_pro_logs' );
delete_site_option( 'wpt_pro_steps' );
delete_site_option( 'wpt_pro_pending_migration' );

// Files
removeRecursiveDir( WP_CONTENT_DIR . '/wp-translations-pro' );

// Transients
delete_site_transient( 'wp_translations_pro_plugin_update' );
$wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE '\_site\_transient\_wpt\_%';" );
$wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE '\_site\_transient\_timeout\_wpt\_%';" );
