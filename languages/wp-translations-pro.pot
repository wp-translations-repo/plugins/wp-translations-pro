# Copyright (C) 2022 WP-Translations Team
# This file is distributed under the same license as the FXB-Translations package.
msgid ""
msgstr ""
"Project-Id-Version: FXB-Translations 1.2.9\n"
"Report-Msgid-Bugs-To: "
"https://wordpress.org/support/plugin/wp-translations-pro\n"
"POT-Creation-Date: 2022-04-07 16:41:00+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2022-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Country: United States\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: "
"__;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_"
"attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-Bookmarks: \n"
"X-Textdomain-Support: yes\n"
"X-Generator: grunt-wp-i18n1.0.2\n"

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:94
msgid "No data available"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:96
msgid "Showing 0 to 0 of 0 entries"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:99
msgid "Processing..."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:100
msgid "Search:"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:101
msgid "No matching records found"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:102
msgid "First"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:103
msgid "Last"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:104
msgid "Next"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:105
msgid "Previous"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:106
msgid ": activate to sort column ascending"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:107
msgid ": activate to sort column descending"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:119
msgid "Save License"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:120
msgid "Delete License"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:121
msgid "Delete Plugin"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:122
#: templates/admin/list-licenses.php:133 templates/admin/list-licenses.php:141
#: templates/admin/list-licenses.php:147
msgid "Activate"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:123
#: templates/admin/list-licenses.php:121
msgid "Deactivate"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:124
#: templates/admin/list-licenses.php:140
msgid "Renew"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:125
#: templates/admin/list-licenses.php:132
msgid "Upgrade"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:126
msgid "Install translations"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:127
msgid "Activated and installed"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:128
msgid "Configure"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:129
msgid "Next Step"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:132
msgid "License expired"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:133
msgid "License activated"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:134
msgid "License can be activated"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:135
msgid "License deactivated"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:136
#: templates/admin/list-licenses.php:100
msgid "unlimited"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:139
msgid "Checking license status"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:140
msgid "Downloading and installing language pack"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:141
msgid "Checking for new updates"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:142
msgid "Clearing update cache"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:143
#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:165
#: src/WP_Translations_Pro/WordPress/Admin/TranslationUpgrader.php:161
msgid "Language pack installed!"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:144
msgid "Deleting license"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:145
msgid "Saving license"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:146
msgid "Deactivating license"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:147
msgid "Deleting old plugin"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:148
msgid "Old plugin deleted"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:157
msgid "Updating translations"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:158
msgid "Translations updated"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:159
msgid "The translations are up to date."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:162
msgid "Downloading and installing your language pack"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:163
#: templates/admin/settings.php:165
msgid "Check for updates"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:164
msgid "Clearing cache"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:179
#: src/WP_Translations_Pro/WordPress/Admin/TranslationNotification.php:76
msgid "New translations are available:&nbsp;"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:180
#: src/WP_Translations_Pro/WordPress/Admin/TranslationNotification.php:77
#: src/WP_Translations_Pro/WordPress/Admin/UpdateCore.php:124
msgid "Update now"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:181
msgid "A premium translation is available in our store!"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:182
#: src/WP_Translations_Pro/WordPress/Helpers/ReadmeHelper.php:22
msgid "View details"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/LicenseActions.php:52
#: src/WP_Translations_Pro/WordPress/Admin/LicenseActions.php:88
#: src/WP_Translations_Pro/WordPress/Admin/LicenseActions.php:118
#: src/WP_Translations_Pro/WordPress/Admin/LicenseActions.php:173
#: src/WP_Translations_Pro/WordPress/Admin/LicenseActions.php:193
#: src/WP_Translations_Pro/WordPress/Admin/LicenseActions.php:256
#: src/WP_Translations_Pro/WordPress/Admin/TranslationUpgrader.php:141
msgid "An error has occurred."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/LicenseActions.php:56
msgid "Please, enter a license key."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/LicenseActions.php:74
msgid "License key successfully saved."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/LicenseActions.php:106
msgid "License key successfully deleted."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/LicenseActions.php:139
msgid "License successfully activated."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/LicenseActions.php:213
msgid "License successfully deactivated."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/LicenseActions.php:220
msgid "License fails to be deactivated."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/LicenseActions.php:264
msgid "Check for updates done."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/Notice.php:33
msgid "Settings updated."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/Notice.php:37
msgid "Products list reloaded."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/Notice.php:41
msgid "Check for translations updates, done!."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/Notice.php:45
msgid "Logs successfully purge."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/Page.php:111
msgid "Visit %1$s website | %2$sContact Support%3$s"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/Page.php:128
msgid "Version:&nbsp;"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/SettingsActions.php:49
msgid "Trying to cheat or something?"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/SettingsActions.php:49
msgid "Error"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/SetupWizard.php:50
msgid ""
"To avoid conflicts we automaticly delete older plugins after migrate "
"licenses keys."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/SetupWizard.php:57
#: src/WP_Translations_Pro/WordPress/Admin/UpdateCore.php:78
#: templates/admin/wizard.php:30
#. translators: Icon type.
msgid "Plugins"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/SetupWizard.php:58
#: templates/admin/_header.php:27 templates/admin/list-licenses.php:32
#: templates/admin/list-licenses.php:187 templates/admin/logs.php:24
#: templates/admin/wizard.php:31
msgid "Licenses"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/SetupWizard.php:59
#: src/WP_Translations_Pro/WordPress/Admin/UpdateCore.php:52
#: templates/admin/list-licenses.php:69 templates/admin/settings.php:45
#: templates/admin/settings.php:124 templates/admin/wizard.php:32
msgid "Actions"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/SetupWizard.php:81
#: templates/admin/wizard.php:56
msgid "Migrate license"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/SetupWizard.php:87
#: templates/admin/wizard.php:62
msgid "Delete old plugin"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/SetupWizard.php:99
msgid "Nothing to migrate"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/SetupWizard.php:131
msgid "Plugin could not be deleted."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/TranslationNotification.php:103
msgid "A premium translation is available!"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/TranslationSetup.php:91
#. translators: 1: Number of updates available to WordPress
msgid "%d WordPress Update"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/TranslationSetup.php:95
#. translators: 1: Number of updates available to plugins
msgid "%d Plugin Update"
msgid_plural "%d Plugin Updates"
msgstr[0] ""
msgstr[1] ""

#: src/WP_Translations_Pro/WordPress/Admin/TranslationSetup.php:99
#. translators: 1: Number of updates available to themes
msgid "%d Theme Update"
msgid_plural "%d Theme Updates"
msgstr[0] ""
msgstr[1] ""

#: src/WP_Translations_Pro/WordPress/Admin/TranslationSetup.php:103
#. translators: 1: Number of updates available to translations
msgid "%d Translation Update"
msgid_plural "%d Translation Updates"
msgstr[0] ""
msgstr[1] ""

#: src/WP_Translations_Pro/WordPress/Admin/TranslationUpgrader.php:35
msgid "You don&#8217;t have permission to do this."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/TranslationUpgrader.php:42
msgid "You do not have sufficient permissions to update this site."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/TranslationUpgrader.php:87
msgid "Unable to connect to the filesystem. Please confirm your credentials."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/TranslationUpgrader.php:97
msgid "Translations update failed."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/TranslationUpgrader.php:164
msgid "Something went wrong with the archive."
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/UpdateCore.php:47
#: templates/admin/logs.php:66
msgid "Text Domain"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/UpdateCore.php:48
msgid "Type"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/UpdateCore.php:49
msgid "Languages"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/UpdateCore.php:50
msgid "Current Version"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/UpdateCore.php:51
msgid "Available Version"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/UpdateCore.php:71
#. translators: Icon type.
msgid "Core"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/UpdateCore.php:86
#. translators: Icon type.
msgid "Themes"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/UpdateCore.php:93
msgid "No translations found"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/UpdateCore.php:117
#: src/WP_Translations_Pro/WordPress/Helpers/ProductHelper.php:111
msgid "Changelog"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Async/WPBackgroundProcess.php:421
msgid "Every %d Minutes"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Cron/LogCron.php:27
msgid "Once Weekly"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Cron/LogCron.php:31
msgid "Once a month"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:41
msgid "Activated"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:46
msgid "Deactivated"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:51
#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:91
msgid "Expired"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:56
msgid "Inactive Site"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:61
msgid "Inactive Key"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:70
msgid "Disabled"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:76
msgid "Invalid"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:81
msgid "Activation Limit"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:86
msgid "Invalid key for this product"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:108
msgid "License Activated"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:113
#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:150
msgid "Your license key expired on %s"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:119
msgid "Your license is not active for this site"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:123
msgid "Inactive License Key"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:132
msgid "Your license key has been disabled"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:137
msgid "Invalid license"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:141
msgid "Your license key has reached its activation limit"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LicenseHelper.php:145
msgid "This appears to be an invalid license key for this product"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LoggerHelper.php:23
msgid "Activation"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LoggerHelper.php:28
msgid "Deactivation"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LoggerHelper.php:33
msgid "Saving"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LoggerHelper.php:38
msgid "Deletion"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LoggerHelper.php:40
msgid "Deleted"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LoggerHelper.php:44
msgid "Updated"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LoggerHelper.php:46
#: src/WP_Translations_Pro/WordPress/Helpers/LoggerHelper.php:60
msgid "Success"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LoggerHelper.php:50
msgid "Update available"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/LoggerHelper.php:52
msgid "Waiting"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/PageHelper.php:20
#: templates/admin/_header.php:30 templates/admin/logs.php:19
#: templates/admin/logs.php:105
msgid "Logs"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/PageHelper.php:25
#: templates/admin/_header.php:36 templates/admin/settings.php:26
#: templates/admin/settings.php:204
msgid "Settings"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/ProductHelper.php:115
msgid "Installation"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/ProductHelper.php:119
#: templates/admin/settings.php:44 templates/admin/settings.php:123
msgid "Description"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/ReadmeHelper.php:39
#: src/WP_Translations_Pro/WordPress/Helpers/ReadmeHelper.php:40
msgid "Close modal"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/ReadmeHelper.php:58
msgid "Version:"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/ReadmeHelper.php:61
msgid "Last Update:"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/ReadmeHelper.php:65
msgid "Contributors:"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Helpers/ReadmeHelper.php:91
msgid "Go to Store"
msgstr ""

#: templates/admin/list-licenses.php:43
msgid "Available translations"
msgstr ""

#: templates/admin/list-licenses.php:54 templates/admin/settings.php:152
msgid "Update Products"
msgstr ""

#: templates/admin/list-licenses.php:65
msgid "Name"
msgstr ""

#: templates/admin/list-licenses.php:66 templates/admin/logs.php:38
msgid "License Key"
msgstr ""

#: templates/admin/list-licenses.php:67
msgid "Activations"
msgstr ""

#: templates/admin/list-licenses.php:68 templates/admin/logs.php:40
msgid "Status"
msgstr ""

#: templates/admin/list-licenses.php:114
msgid "Save license"
msgstr ""

#: templates/admin/list-licenses.php:115
msgid "Get a license"
msgstr ""

#: templates/admin/list-licenses.php:150
msgid "Delete license"
msgstr ""

#: templates/admin/list-licenses.php:194
msgid "Products"
msgstr ""

#: templates/admin/logs.php:27
msgid "Updates"
msgstr ""

#: templates/admin/logs.php:37 templates/admin/logs.php:65
msgid "Date"
msgstr ""

#: templates/admin/logs.php:39 templates/admin/logs.php:69
msgid "Action"
msgstr ""

#: templates/admin/logs.php:67
msgid "Language"
msgstr ""

#: templates/admin/logs.php:68
msgid "Version"
msgstr ""

#: templates/admin/settings.php:31
msgid "UI Integration"
msgstr ""

#: templates/admin/settings.php:34
msgid "Advanced"
msgstr ""

#: templates/admin/settings.php:43 templates/admin/settings.php:122
msgid "Options"
msgstr ""

#: templates/admin/settings.php:52
msgid "Core updates"
msgstr ""

#: templates/admin/settings.php:56
msgid "Display translations updates details in core's update page."
msgstr ""

#: templates/admin/settings.php:64
msgid "Plugins updates"
msgstr ""

#: templates/admin/settings.php:68
msgid "Display translations updates details in plugin's update page."
msgstr ""

#: templates/admin/settings.php:76
msgid "Themes updates"
msgstr ""

#: templates/admin/settings.php:80
msgid "Display translations updates details in theme's update page."
msgstr ""

#: templates/admin/settings.php:88
msgid "Bubble count"
msgstr ""

#: templates/admin/settings.php:92
msgid "Display translations updates in the bubble menu bar."
msgstr ""

#: templates/admin/settings.php:100
msgid "Admin page position"
msgstr ""

#: templates/admin/settings.php:108
msgid "Dedicated page"
msgstr ""

#: templates/admin/settings.php:109
msgid "Settings subpage"
msgstr ""

#: templates/admin/settings.php:132
msgid "Debug Mode"
msgstr ""

#: templates/admin/settings.php:136
msgid "Toggle debug mode, it may affect performance."
msgstr ""

#: templates/admin/settings.php:145
msgid "Products Update"
msgstr ""

#: templates/admin/settings.php:149
msgid "Force products update from stores."
msgstr ""

#: templates/admin/settings.php:158
msgid "Check Translations Updates"
msgstr ""

#: templates/admin/settings.php:162
msgid "Force translations updates from stores."
msgstr ""

#: templates/admin/settings.php:171
msgid "Purge Logs"
msgstr ""

#: templates/admin/settings.php:178
msgid "Purge"
msgstr ""

#: templates/admin/settings.php:191
msgid "Save Settings"
msgstr ""

#: templates/admin/wizard.php:24
msgid "Migration Wizard"
msgstr ""

#: templates/admin/wizard.php:25
msgid "The migration of your old products is necessary before continuing."
msgstr ""

#: wp-translations-pro.php:83
msgid ""
"<strong>%1$s</strong> requires PHP %2$s minimum, your website is actually "
"running version %3$s."
msgstr ""

#. Plugin Name of the plugin/theme
msgid "FXB-Translations"
msgstr ""

#. Description of the plugin/theme
msgid ""
"The easiest way to manage the French Translations of your favorite "
"WordPress themes and plugins provided by FX Bénard"
msgstr ""

#. Author of the plugin/theme
msgid "WP-Translations Team"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://wp-translations.pro"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:95
msgctxt "Please don't translate: _START_ _END_ _TOTAL_"
msgid "Showing _START_ to _END_ of _TOTAL_ entries"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:97
msgctxt "Please don't translate: _MAX_"
msgid "filtered from _MAX_ total entries"
msgstr ""

#: src/WP_Translations_Pro/WordPress/Admin/EnqueueAssets.php:98
msgctxt "Please don't translate: _MENU_"
msgid "Show _MENU_ entries"
msgstr ""